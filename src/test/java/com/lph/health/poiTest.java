package com.lph.health;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileInputStream;

public class poiTest {
    // 使用POI读取excel文件
    @Test
    public void test() throws Exception {
        // 加载指定文件创建一个excel对象 工作簿
        XSSFWorkbook excel = new XSSFWorkbook(new FileInputStream(new File("E:\\黑马程序员\\第4阶段企业级开发—项目实战\\1、传智健康项目\\资料-传智健康项目\\day05\\资源\\预约设置模板文件\\test.xlsx")));
        // 读取Excel标签页中第一个sheet标签页 工作表
        XSSFSheet sheet = excel.getSheetAt(0);
        // 遍历 sheet
        sheet.forEach(row ->{
            //  遍历行获得每个单元格
            row.forEach(cell -> {
                System.out.println(cell.getStringCellValue());
            });
        });
        excel.close();
    }
    @Test
    public void test1() throws Exception {
        // 加载指定文件创建一个excel对象 工作簿
        XSSFWorkbook excel = new XSSFWorkbook(new FileInputStream(new File("E:\\黑马程序员\\第4阶段企业级开发—项目实战\\1、传智健康项目\\资料-传智健康项目\\day05\\资源\\预约设置模板文件\\test.xlsx")));
        // 读取Excel标签页中第一个sheet标签页 工作表
        XSSFSheet sheet = excel.getSheetAt(0);
        // 获得工作表最后一个行号 0为第一行
        int lastRowNum = sheet.getLastRowNum();

        for (int i = 0; i <= lastRowNum; i++){
            XSSFRow rows = sheet.getRow(i); // 根据行号获取每一行
            short lastCellNum = rows.getLastCellNum(); // 获取当前行最后一个单元格索引
            for (short j = 0; j < lastCellNum; j++) {
                XSSFCell cell = rows.getCell(j); // 根据单元格索引获取单元格对象
                System.out.println(cell.getStringCellValue());
            }
        }
        excel.close();
    }
}
