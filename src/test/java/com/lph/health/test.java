package com.lph.health;

import com.lph.health.utils.*;
import com.lph.health.vo.MailVo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

@SpringBootTest
public class test {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private MailUtil mailUtil;

    @Test
    void contextLoads(){
        // 由md5 加密后再由 passwordEncoder再次加密
        //System.out.println(passwordEncoder.encode(MD5Util.md5("123456")));

        // $2a$10$na5lBwwHdB2GJ/aiOzdxMeBtsW50Y0/GEIBLUrV28nT21FTtpjYZW
        //boolean matches = passwordEncoder.matches(MD5Util.md5("123456"), passwordEncoder.encode(MD5Util.md5("123456")));
        String uuid = UUID.randomUUID().toString().substring(0, 7);
        String orderId = MakeOrderNumUtil.getOrderId();
        System.out.println(orderId);
        System.out.println(uuid);
    }

    @Test
    void testMail(){
        MailVo mailVo = new MailVo();
        mailVo.setReceivers(new String[]{"1710193472@qq.com"});
        mailVo.setSubject("健康计划定制服务平台");

        String cod4 = ValidateCodeUtil.generateValidateCode(4).toString();

        mailVo.setContent("邮件发送测试----你的验证码为 {"+ cod4 +"} 请注意查收");
        System.out.println(mailUtil.sendMail(mailVo));
    }

    @Test
    void testHtmlMail(){
        MailVo mailVo = new MailVo();
        mailVo.setHtml(true);
        mailVo.setReceivers(new String[]{"moqianqingtanmqqt@163.com"});
        mailVo.setSubject("健康计划定制服务平台");
        mailVo.setContent("<a href='https://www.baidu.com' style='color: red'>邮件发送测试</a>");
        System.out.println(mailUtil.sendMail(mailVo));
    }

    /**
     * 测试  Calendar 对象获取当前时间往前12个月的时间
     */
    @Test
    void testGetMonth() throws Exception {
        Calendar calendar = Calendar.getInstance();// 获取日历对象,默认为当前系统时间
        System.out.println(DateUtil.parseDate2String(calendar.getTime()));
        // 计算过去一年的12个月,从当月开始
        calendar.add(Calendar.MONTH, -12);// 按月份，往前推12个月
        for (int i = 0; i < 12; i++) {
            calendar.add(Calendar.MONTH, 1);// 获得当前时间(去年)往后推一个月
            Date time = calendar.getTime();// 获得日期
            System.out.println(DateUtil.parseDate2String(time));
        }
    }
}
