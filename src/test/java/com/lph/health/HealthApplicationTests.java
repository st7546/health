package com.lph.health;

import com.lph.health.dao.SysPermissionMapper;
import com.lph.health.dao.SysUserMapper;
import com.lph.health.entity.SysPermission;
import com.lph.health.entity.SysUser;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class HealthApplicationTests {
    @Autowired
    private SysUserMapper sysUserMapper;
    @Autowired
    private SysPermissionMapper sysPermissionMapper;

//    @Test
//    public void testInsert() {
//        SysUser sysUser = new SysUser();
//        sysUser.setNickName("张三");
//
//        int result = sysUserMapper.insert(sysUser);
//
//        System.out.println("受影响的行数:"+result);
//        System.out.println(sysUser);
//    }

    @Test
    public void testInsertSP() {
        SysPermission sysPermission = new SysPermission();
        sysPermission.setLabel("张三");

        sysPermissionMapper.insert(sysPermission);

    }

//    @Test
//    public void testUpdate() {
//        SysUser sysUser = new SysUser();
//        sysUser.setNickName("李四");
//
//        int result = sysUserMapper.updateById(sysUser);
//
//        System.out.println("受影响的行数:"+result);
//        System.out.println(sysUser);
//    }
}
