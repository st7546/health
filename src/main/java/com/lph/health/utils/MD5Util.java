package com.lph.health.utils;

import lombok.extern.slf4j.Slf4j;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * MD5加密工作类
 * md5为单向加密 不可逆
 */
@Slf4j
public class MD5Util {

    /**
     * md5加密
     * @param password 要加密的内容
     * @return 返回一个32位的加密串
     */
    public static String md5(String password) {
        if (StringUtil.isNotEmpty(password)){
            byte[] bytes = null;
            try {
                bytes = MessageDigest.getInstance("md5").digest(password.getBytes());
            } catch (NoSuchAlgorithmException e) {
                log.error("没有md5这个加密算法");
            }
            // 由md5加密算法得到字节数组转换为16进制数字
            StringBuilder code = new StringBuilder(new BigInteger(1, bytes).toString(16));
            // 需要保证加密后为32位
            for (int i = 0; i < 32 - code.length(); i++) {
                code.insert(0, "0");
            }
            return code.toString();
        } else {
            return null;
        }
    }

}
