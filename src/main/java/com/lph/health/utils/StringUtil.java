package com.lph.health.utils;

/**
 * 字符串判断
 */
public class StringUtil {
    /**
     * 字符串判断
     * @param str
     * @return
     */
    public static boolean isNotEmpty(String str) {
        return str != null && !"".equals(str);
    }
}
