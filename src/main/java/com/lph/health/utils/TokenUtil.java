package com.lph.health.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * token工具类
 * 用于生成token
 * 用户登录拿到token然后访问系统资源
 */

@Component  //加入spring 容器
public class TokenUtil {

    @Value("${jwt.secret}")
    private String secret;

    @Value("${jwt.expiration}")
    private long expiration;

    /**
     * 传入用户信息，生成token
     * @param details
     * @return
     */
    public String generateToken(UserDetails details){
        Map<String, Object> map = new HashMap<>(2);
        map.put("userName", details.getUsername());
        map.put("created", new Date());

        return this.generateJwt(map);
    }

    /**
     * 根据荷载信息生成token
     * @param map
     * @return
     */
    private String generateJwt(Map<String, Object> map){
        return Jwts.builder()
                   .setClaims(map)
                   .signWith(SignatureAlgorithm.HS256, secret)
                   .setExpiration(new Date(System.currentTimeMillis() + expiration * 1000))
                   .compact();
    }

    /**
     * 根据token获取荷载信息
     * @param token
     * @return
     */
    public Claims getTokenBody(String token){
        try{
            return Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();
        } catch (Exception e){
            return null;
        }
    }

    /**
     * 根据token得到用户名
     * @param token
     * @return
     */
    public String getUserNameByToken(String token){
        return (String) this.getTokenBody(token).get("userName"); //获取用户信息
    }

    /**
     * 根据token ，判断该token在当前时间内是否过期
     * @param token
     * @return
     */
    public boolean isExpiration(String token){
        return this.getTokenBody(token).getExpiration().before(new Date());
    }

    /**
     * 刷新token
     * @param token
     * @return
     */
    public String refreshToken(String token){
        Claims claims = this.getTokenBody(token);
        claims.setExpiration(new Date());
        return this.generateJwt(claims);
    }
}
