package com.lph.health.utils;

import com.aliyun.dysmsapi20170525.Client;
import com.aliyun.dysmsapi20170525.models.SendSmsRequest;
import com.aliyun.teaopenapi.models.Config;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * 阿里云短信工具类
 */
@Slf4j
@Component
public class SmsUtils {

    @Value("${aliyun.accessKey}")
    private String accessKey;

    @Value("${aliyun.secretKey}")
    private String secretKey;

    @Value("${aliyun.signName}")
    private String signName;

    @Value("${aliyun.templateCode}")
    private String templateCode;

    /**
     * 构建短信发送连接
     * @return 短信的连接
     */
    public Client createClient() throws Exception {
        Config config = new Config()
                .setAccessKeyId(accessKey)
                .setAccessKeySecret(secretKey);
        config.endpoint = "dysmsapi.aliyuncs.com";
        return new Client(config);
    }

    /**
     * 传入电话号码 发送短信
     * @param phoneNumbers
     * @return
     */
    public void sendSms(String phoneNumbers){
        try {
            Integer code = ValidateCodeUtil.generateValidateCode(6);
            Client client = this.createClient();
            SendSmsRequest request = new SendSmsRequest()
                    .setPhoneNumbers(phoneNumbers)
                    .setSignName(signName)
                    .setTemplateCode(templateCode)
                    .setTemplateParam("{code: " +code+ "}");
            //.setTemplateParam("{\"code\":\" " +code+ "\"}"); // json对象格式
            client.sendSms(request);
            log.info("发送短信验证码成功-->{}", code);
        } catch (Exception e) {
            log.error("短信发送失败-->{}", e.getMessage());
        }

    }
}
