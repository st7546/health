package com.lph.health.utils;

import com.lph.health.entity.SysUser;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * 用于获取当前登录用户的基本信息
 */
public class SecurityUtil {

    /**
     * 从security主体中获取用户信息
     * 注意隐藏用户密码
     * @return
     */
    public static SysUser getUser(){
//        return  (SysUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal(); //原版
        SysUser sysUser = (SysUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        sysUser.setPassword(null);  //  清空密码
        sysUser.setName(sysUser.getUsername());
        return sysUser;
    }

    /**
     * 在security中获取用户名
     * @return
     */
    public static String getUserName(){
        return getUser().getUsername();
    }

    /**
     * 在security中获取用户ID
     * @return
     */
    public static Long getUserId(){
        return getUser().getId();
    }
}
