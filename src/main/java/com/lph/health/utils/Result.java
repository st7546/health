package com.lph.health.utils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 返回前端的结果
 */

@Data //get set方法注解 Lombok插件
@ApiModel(value = "响应参数")
public class Result implements Serializable { //Serializable 序列化

    @ApiModelProperty(value = "是否成功的标识", dataType = "boolean")
    private boolean flag; //是否成功的标志 (响应参数）

    @ApiModelProperty(value = "响应消息", dataType = "String")
    private String message; //响应消息

    @ApiModelProperty(value = "响应数据", dataType = "Object")
    private Object data; //数据

    public Result() {
    }

    public Result(boolean flag, String message, Object data) {
        this.flag = flag;
        this.message = message;
        this.data = data;
    }

    public Result(boolean flag, String message) {
        this.flag = flag;
        this.message = message;
    }

    /**
     * 响应成功
     * @param message
     * @param data
     * @return
     */
    //构造器
    public static Result success(String message, Object data){
        return new Result(true, message, data);
    }

    //
    public static Result success(String message){
        return new Result(true, message);
    }

    /**
     * 响应失败
     * @param message
     * @return
     */
    public static Result failure(String message){
        return new Result(false, message);
    }
}
