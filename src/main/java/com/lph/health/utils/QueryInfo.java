package com.lph.health.utils;

import lombok.Data;

/**
 * 分页查询
 */

@Data
public class QueryInfo {

    private Integer pageNumber; //第几页

    private Integer pageSize; //分页大小（数据数量）

    private String queryString; //查询内容

}
