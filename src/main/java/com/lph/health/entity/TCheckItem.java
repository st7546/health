package com.lph.health.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-10-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class TCheckItem implements Serializable {

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "项目编码")
    private String code;

    @ApiModelProperty(value = "检查项名称")
    private String name;

    @ApiModelProperty(value = "适用性别")
    private String sex;

    @ApiModelProperty(value = "适用年龄")
    private String age;

    @ApiModelProperty(value = "查检项类型,分为检查和检验两种")
    private String type;

    @ApiModelProperty(value = "检查项说明")
    private String remark;

    @ApiModelProperty(value = "注意事项")
    private String attention;

}
