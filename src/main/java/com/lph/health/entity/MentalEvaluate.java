package com.lph.health.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-11-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class MentalEvaluate implements Serializable {

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "评估日期")
    private Date evDate;

    @ApiModelProperty(value = "健康员")
    private String assessor;

    @ApiModelProperty(value = "躯体化程度")
    private String somatization;

    @ApiModelProperty(value = "强迫症状程度")
    private String obco;

    @ApiModelProperty(value = "人际关系敏感度")
    private String interSen;

    @ApiModelProperty(value = "抑郁程度")
    private String depression;

    @ApiModelProperty(value = "焦虑程度")
    private String anxiety;

    @ApiModelProperty(value = "敌对程度")
    private String hostility;

    @ApiModelProperty(value = "恐怖程度")
    private String phobicAn;

    @ApiModelProperty(value = "偏执程度")
    private String parIdeation;

    @ApiModelProperty(value = "精神病性程度")
    private String psychoticism;

    @ApiModelProperty(value = "评估说明")
    private String remark;

    @ApiModelProperty(value = "预约编号")
    private Long orderId;


}
