package com.lph.health.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.lph.health.utils.StringUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-10-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class    SysUser implements Serializable,  UserDetails{

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "用户名")
    private String userName;

    @ApiModelProperty(value = "前端展示的用户名")
    private String name;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "昵称")
    private String nickName;

    @ApiModelProperty(value = "头像")
    private String avatar;

    @ApiModelProperty(value = "性别")
    private Boolean sex;

    @ApiModelProperty(value = "地址")
    private String address;

    @ApiModelProperty(value = "微信唯一id")
    private String openId;

    @ApiModelProperty(value = "当前状态")
    private Boolean status;

    @ApiModelProperty(value = "是否是管理员")
    private Boolean admin;

    @ApiModelProperty(value = "是否是普通会员")
    private Boolean member;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @ApiModelProperty(value = "用户对应角色")
    private List<SysRole> roles;

    @ApiModelProperty(value = "用户对应的菜单列表")
    private List<SysMenu> menus;

    @ApiModelProperty(value = "用户对应的数据权限")
    private List<SysPermission> permissions;

    @ApiModelProperty(value = "普通用户对应的会员档案")
    private TMember members;
    /**
     * 权限数据
     * @return
     */
    @Override
    @JsonIgnore
    public Collection<? extends GrantedAuthority> getAuthorities() {
        // 设置角色信息
        List<GrantedAuthority> list = new ArrayList<>();
        if(roles != null && roles.size() > 0){
            roles.forEach(item -> {
                if (StringUtil.isNotEmpty(item.getLabel())) {
                    list.add(new SimpleGrantedAuthority("ROLE_" + item.getValue()));
                }
            });
        }
        // 设置权限信息
        if(permissions != null && permissions.size() > 0){
            permissions.forEach(item ->{
                if (StringUtil.isNotEmpty(item.getLabel())) {
                    list.add(new SimpleGrantedAuthority(item.getValue()));
                }
            });
        }

        return list;
    }

    /**
     * 用户名
     * @return
     */
    @Override
    @JsonIgnore
    public String getUsername() {
        return userName;
    }

    /**
     * 账号是否过期
     * @return
     */
    @Override
    @JsonIgnore
    public boolean isAccountNonExpired() {
        return false;
    }

    /**
     * 账号是否被锁定
     * @return
     */
    @Override
    @JsonIgnore
    public boolean isAccountNonLocked() {
        return false;
    }

    /**
     * 凭证是否过期
     * @return
     */
    @Override
    @JsonIgnore
    public boolean isCredentialsNonExpired() {
        return false;
    }

    /**
     * 是否被禁用
     * @return
     */
    @Override
    @JsonIgnore
    public boolean isEnabled() {
        return status;
    }


}
