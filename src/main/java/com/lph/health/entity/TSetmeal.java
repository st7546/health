package com.lph.health.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-10-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class TSetmeal implements Serializable {


    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "套餐名称")
    private String name;

    @ApiModelProperty(value = "套餐编号")
    private String code;

    @ApiModelProperty(value = "助记码")
    @TableField("helpCode")
    private String helpCode;

    @ApiModelProperty(value = "适用性别")
    private String sex;

    @ApiModelProperty(value = "适用年龄")
    private String age;

    @ApiModelProperty(value = "套餐说明")
    private String remark;

    @ApiModelProperty(value = "注意事项")
    private String attention;

    @ApiModelProperty(value = "图片路径")
    private String img;

    @ApiModelProperty(value = "检查组")
    private List<TCheckGroup> checkGroups;

    @ApiModelProperty(value = "检查项")
    private List<TCheckItem> checkItems;
}
