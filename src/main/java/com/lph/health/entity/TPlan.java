package com.lph.health.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-11-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)

public class TPlan implements Serializable {

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "健康员")
    private String assessor;

    @ApiModelProperty(value = "方案标题")
    private String title;

    @ApiModelProperty(value = "方案开始时间")
    private Date planUp;

    @ApiModelProperty(value = "方案结束时间")
    private Date planEnd;

    @ApiModelProperty(value = "方案目标")
    private String planGoal;

    @ApiModelProperty(value = "方案执行人")
    private String planUser;

    @ApiModelProperty(value = "方案说明")
    private String remark;

    @ApiModelProperty(value = "预约编号")
    private Long orderId;

    @ApiModelProperty(value = "食物方案分配状态")
    private String foodStatus;

    @ApiModelProperty("方案计划下的食物方案")
    private List<PlanFood> planFoods;

    @ApiModelProperty("方案计划下的食物")
    private List<Food> foods;
}
