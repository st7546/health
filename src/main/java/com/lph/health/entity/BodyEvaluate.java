package com.lph.health.entity;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 体质评估
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-11-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Api(value="体质评估")
public class BodyEvaluate implements Serializable {

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "体质类型")
    private String bodyType;

    @ApiModelProperty(value = "评估人")
    private String assessor;

    @ApiModelProperty(value = "评估日期")
    private Date evDate;

    @ApiModelProperty(value = "说明")
    private String remark;

    @ApiModelProperty(value = "预约编号")
    private Long orderId;


}
