package com.lph.health.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-11-01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class TOrderSetting implements Serializable {

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "约预日期")
    private Date orderDate;

    @ApiModelProperty(value = "可预约人数")
    private Integer number;

    @ApiModelProperty(value = "已预约人数")
    private Integer reservations;


    public TOrderSetting(Date orderDate, int number){
        this.orderDate = orderDate;
        this.number = number;
    }
}
