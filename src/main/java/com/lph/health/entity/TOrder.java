package com.lph.health.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-11-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class TOrder implements Serializable {

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "员会id")
    private Long memberId;

    @ApiModelProperty(value = "约预日期")
    private Date orderDate;

    @ApiModelProperty(value = "约预类型 线下预约/微信预约")
    private String orderType;

    @ApiModelProperty(value = "预约状态（是否到诊）")
    private String orderStatus;

    @ApiModelProperty(value = "餐套id")
    private Long setmealId;

    @ApiModelProperty(value = "评估状态")
    private String evStatus;

    @ApiModelProperty(value = "体质评估状态")
    private String boEvStatus;

    @ApiModelProperty(value = "心理评估状态")
    private String meEvStatus;

    @ApiModelProperty(value = "体检评估状态")
    private String bdStatus;

    public TOrder(Long id, Long memberId, Date orderDate, String orderType, String orderStatus, Long setmealId) {
        this.id = id;
        this.memberId = memberId;
        this.orderDate = orderDate;
        this.orderType = orderType;
        this.orderStatus = orderStatus;
        this.setmealId = setmealId;
    }

    public TOrder() {
    }

    public TOrder(Long memberId, Date orderDate, Long setmealId) {
        this.memberId = memberId;
        this.orderDate = orderDate;
        this.setmealId = setmealId;
    }
}
