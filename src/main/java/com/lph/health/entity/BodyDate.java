package com.lph.health.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-11-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class BodyDate implements Serializable {

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "体检报告图片")
    private String imgUrl;

    private Date evDate;

    @ApiModelProperty(value = "健康员")
    private String assessor;

    @ApiModelProperty(value = "风险预测")
    private String risk;

    @ApiModelProperty(value = "评估说明")
    private String remark;

    @ApiModelProperty(value = "预约编号")
    private Long orderId;


}
