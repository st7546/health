package com.lph.health.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-11-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class PlanFood implements Serializable {

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "使用方案")
    private String instructions;

    @ApiModelProperty(value = "使用建议")
    private String suggestion   ;

    @ApiModelProperty(value = "方案效益")
    private String benefit;

    @ApiModelProperty("方案计划下的食物")
    private List<Food> foods;
}
