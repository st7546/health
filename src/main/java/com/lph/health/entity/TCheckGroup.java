package com.lph.health.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-10-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class TCheckGroup implements Serializable {

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "检查组编号")
    private String code;

    @ApiModelProperty(value = "检查组名称")
    private String name;

    @ApiModelProperty(value = "助记码")
    private String helpCode;

    @ApiModelProperty(value = "适用性别")
    private String sex;

    @ApiModelProperty(value = "说明")
    private String remark;

    @ApiModelProperty(value = "检查项")
    private List<TCheckItem> checkItems;

    @ApiModelProperty(value = "注意事项")
    private String attention;
}
