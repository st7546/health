package com.lph.health.dao;

import com.github.pagehelper.Page;
import com.lph.health.entity.*;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-11-21
 */
@Repository
public interface TPlanMapper{
    /**
     * 添加方案计划
     * @param tPlan
     */
    void insert(TPlan tPlan);

    /**
     * 删除方案计划
     * @param id
     */
    void delete(Long id);

    /**
     * 修改方案计划
     * @param tPlan
     */
    void update(TPlan tPlan);

    /**
     * 分页查询
     * @param queryString
     * @return
     */
    Page<TPlan> findPage(String queryString);

    /**
     * 根据分类名称查询方案计划
     * @param title
     * @return
     */
    TPlan findByTitle(String title);

    /**
     * 查询未分配食物方案的计划
     * @return
     */
    List<TPlan> findFoodStatus();

    /**
     * 获取所有的方案计划
     * @return
     */
    List<TPlan> planAll();

    /**
     * 根据计划Id查询计划信息
     * @param id
     * @return
     */
    TPlan findPlanById(@Param("value") Long id);

    /**
     * 根据套餐Id 查询套餐信息
     * @param id
     * @return
     */
    TSetmeal findBySetId(Long id);

    /**
     * 根据套餐Id 查询会员信息
     * @param id
     * @return
     */
    TMember findByMerId(Long id);
}
