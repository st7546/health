package com.lph.health.dao;

import com.lph.health.entity.TOrderSetting;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  预约限制
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-10-26
 */
@Repository
public interface TOrderSettingMapper {

    /**
     * 添加预约限制
     * @param tOrderSetting 套餐数据
     */
    void add(TOrderSetting tOrderSetting);

    /**
     * 根据日期更新预约人数
     * @param tOrderSetting 权限数据
     */
    void editNumberByOrderDate(TOrderSetting tOrderSetting);

    /**
     * 更新已预约人数
     * @param tOrderSetting
     */
    void editReservationsByOrderDate(TOrderSetting tOrderSetting);

    /**
     * 根据预约日期查询
     * @param orderDate
     * @return
     */
    Long findCountByOrderDate(Date orderDate);

    Long findCountByOrderDate1(String orderDate);

    /**
     * 根据年月查询预约设置信息
     * @param map
     * @return
     */
    List<TOrderSetting> getOrderSettingByMonth(Map<String, String> map);

    /**
     * 根据预约日期查询预约设置信息
     * @param orderDate
     * @return
     */
    TOrderSetting findByOrderDate(Date orderDate);
}
