package com.lph.health.dao;

import com.github.pagehelper.Page;
import com.lph.health.entity.Sport;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-12-11
 */
@Repository
public interface SportMapper{

    /**
     * 添加运动资讯信息
     * @param sport 权限数据
     */
    void insert(Sport sport);

    /**
     * 修改运动资讯信息
     * @param sport 运动资讯数据
     */
    void update(Sport sport);

    /**
     * 删除运动资讯数据
     * @param id id
     */
    void delete(Long id);

    /**
     * 分页查询
     * @param queryString
     * @return
     */
    Page<Sport> findPage(String queryString);

    Sport findByTitle(String title);
}
