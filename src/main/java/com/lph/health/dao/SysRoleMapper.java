package com.lph.health.dao;


import com.github.pagehelper.Page;
import com.lph.health.entity.SysRole;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * <p>
 *  角色数据的增删改查
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-10-15
 */
@Repository
public interface SysRoleMapper {

    /**
     * 添加角色息
     * @param sysRole 角色数据
     */
    void insert(SysRole sysRole);

    /**
     * 修改角色信息
     * @param sysRole 权限数据
     */
    void update(SysRole sysRole);

    /**
     * 删除角色数据
     * @param id id
     */
    void delete(Long id);

    /**
     * 分页查询
     * @param queryString
     * @return
     */
    Page<SysRole> findPage(String queryString);

    /**
     * 根据角色Id 查询角色信息
     * @param id
     * @return
     */
    SysRole findById(Long id);

    /**
     * 根据角色id 删除对应菜单数据
     * @param roleId
     */
    void deleteMenuByRoleId(@Param("roleId") Long roleId);

    /**
     * 根据角色id 删除对应权限数据
     * @param roleId
     */
    void deletePerByRoleId(@Param("roleId") Long roleId);

    /**
     * 在角色权限关系表中添加
     * @param roleId
     * @param permissionId
     */
    void insertPermissions(@Param("roleId") Long roleId, @Param("permissionId") Long permissionId);

    /**
     * 在角色菜单关系表中添加
     * @param roleId
     * @param menuId
     */
    void insertMenus(@Param("roleId") Long roleId, @Param("menuId") Long menuId);

    /**
     * 根据角色标签查询角色信息
     * @param label
     * @return
     */
    SysRole findByLabel(String label);

    /**
     * 查询所有角色信息
     * @return
     */
    List<SysRole> findAll();
}
