package com.lph.health.dao;


import com.github.pagehelper.Page;
import com.lph.health.entity.TCheckItem;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-10-26
 */
@Repository
public interface TCheckItemMapper {

    /**
     * 添加检查项信息
     * @param tCheckItem 检查项数据
     */
    void insert(TCheckItem tCheckItem);

    /**
     * 修改检查项信息
     * @param tCheckItem 检查项数据
     */
    void update(TCheckItem tCheckItem);

    /**
     * 删除检查项数据
     * @param id id
     */
    void delete(Long id);

    /**
     * 分页查询
     * @param queryString
     * @return
     */
    Page<TCheckItem> findPage(String queryString);

    /**
     * 根据检查组id查询检查项id信息
     * @param checkGroupId
     * @return
     */
    List<TCheckItem> findByCheckItemId(@Param("checkGroupId") Long checkGroupId);

    /**
     * 查询所有检查项信息
     * @return
     */
    List<TCheckItem> findAll();
}
