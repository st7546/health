package com.lph.health.dao;


import com.github.pagehelper.Page;
import com.lph.health.entity.SysMenu;
import com.lph.health.entity.SysPermission;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * <p>
 *  菜单数据的增删改查
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-10-15
 */

@Repository
public interface SysMenuMapper {

    /**
     * 添加菜单息
     * @param sysMenu 菜单数据
     */
    void insert(SysMenu sysMenu);

    /**
     * 修改菜单信息
     * @param sysMenu 权限数据
     */
    void update(SysMenu sysMenu);

    /**
     * 删除菜单数据
     * @param id id
     */
    void delete(Long id);

    /**
     * 分页查询
     * @param queryString
     * @return
     */
    Page<SysMenu> findPage(String queryString);

    /**
     * 查询所有父级菜单
     * @return
     */
    List<SysMenu> findParent();

    /**
     * 根据角色id查询父级菜单信息
     * @param roleId
     * @return
     */
    List<SysMenu> findByRoleId(@Param("roleId") Long roleId);

    /**
     * 根据角色id和父级id查询子菜单信息
     * @param parentId
     * @param roleId
     * @return
     */
    List<SysMenu> findByRoleIdAndParentId(@Param("parentId") Long parentId, @Param("roleId") Long roleId);

}
