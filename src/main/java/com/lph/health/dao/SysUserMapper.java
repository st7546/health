package com.lph.health.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.github.pagehelper.Page;
import com.lph.health.entity.*;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository //代表持久层
public interface SysUserMapper extends BaseMapper<SysUser> {
    /**
     * 查询用户信息
     * @return
     */
    List<SysUser> findAll();

    /**
     * 根据用户民获取用户对象
     * @return
     */
    SysUser findUserName(String userName);

    /**
     * 根据用户民获取用户对象
     * @return
     */
    SysUser findByUserName(@Param("value") String value);

    /**
     * 根据角色Id 查询角色信息
     * @param id
     * @return
     */
    SysUser findById(Long id);

    SysUser findByEmail(String email);

    /**
     * 根据用户ID获取角色信息
     * @return
     */
    List<SysRole> findRoles(@Param("userId") Long userId);

    /**
     * 根据用户id查询菜单信息
     * @param userId
     * @return
     */
    List<SysMenu> findMenus(@Param("userId") Long userId);

    /**
     * 根据用户id查询权限数据
     * @param userId
     * @return
     */
    List<SysPermission> findPermissions(@Param("userId") Long userId);

    /**
     * 根据用户id查询普通档案信息
     * @param userId
     * @return
     */
    TMember findMembers(@Param("userId") Long userId);

    /**
     * 根据父级id查询子菜单信息
     * @param id
     * @param userId
     * @return
     */
    List<SysMenu> findChildrenMenu(@Param("id") Long id, @Param("userId") Long userId);

    /**
     * 分页查询
     * @param queryString 分页查询条件
     * @return
     */
    Page<SysUser> findPage(String queryString);

    /**
     *
     */
    void insertUserMember(Long userNewId, Long memberNewId);

    /**
     * 添加用户息
     * @param sysUser 用户数据
     */
    void insertUser(SysUser sysUser);

    /**
     * 修改用户信息
     * @param sysUser 用户信息
     */
    void updateUser(SysUser sysUser);

    /**
     * 删除用户数据
     * @param id id
     */
    void deleteUser(Long id);

    /**
     * 添加用户角色信息(在用户角色关系表中添加)
     * @param userId
     * @param roleId
     */
    void insertUserRole(@Param("userId") Long userId, @Param("roleId") Long roleId);

    /**
     * 删除用户角色信息(在用户角色关系表中删除)
     * @param userId
     */
    void deleteRoleByUserId(@Param("userId") Long userId);

    /**
     *
     * @param email
     * @param password
     */
    @Update("update sys_user")
    void upPwdByMail(@Param("email") String email, @Param("password") String password);

    /**
     * 查询所有角色为健康员的用户名
     * @return
     */
    List<SysUser> findAssessor();
}
