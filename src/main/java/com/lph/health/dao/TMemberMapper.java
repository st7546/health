package com.lph.health.dao;


import com.github.pagehelper.Page;
import com.lph.health.entity.TMember;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-11-13
 */
@Repository
public interface TMemberMapper{
    /**
     * 查询所有会员信息
     * @return
     */
    List<TMember> findAll();

    /**
     * 分页查询
     * @param queryString
     * @return
     */
    Page<TMember> findPage(String queryString);

    /**
     * 添加会员信息
     * @param tMember 会员信息数据
     */
    void insert(TMember tMember);

    /**
     * 修改会员信息
     * @param tMember
     */
    void update(TMember tMember);

    /**
     * 删除会员信息数据
     * @param id id
     */
    void delete(Long id);

    /**
     * 根据会员Id
     * @param id
     * @return
     */
    TMember findById(Long id);

    /**
     *
     * @param email
     * @return
     */
    TMember findByEmail(String email);

    /**
     * 根据日期统计会员数，统计指定日期之前的会员数
     * @param date
     * @return
     */
    Integer findMemberCountBeforeDate(String date);

    /**
     * 根据日期统计会员数
     * @param date
     * @return
     */
    Integer findMemberCountByDate(String date);

    /**
     * 根据日期统计会员数，统计指定日期之后的会员数
     * @param date
     * @return
     */
    Integer findMemberCountAfterDate(String date);

    /**
     * 总会员数
     * @return
     */
    Integer findMemberTotalCount();

    /**
     * 用于判断该身份证号是否已被注册
     * @return
     */
    TMember findByIdCard(@Param("icCard") String idCard);
}
