package com.lph.health.dao;


import com.github.pagehelper.Page;
import com.lph.health.entity.TCheckGroup;
import com.lph.health.entity.TCheckItem;
import com.lph.health.entity.TSetmeal;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-10-26
 */
@Repository
public interface TSetmealMapper {

    /**
     * 添加套餐息
     * @param tSetmeal 套餐数据
     */
    void insert(TSetmeal tSetmeal);

    /**
     * 修改套餐信息
     * @param tSetmeal 权限数据
     */
    void update(TSetmeal tSetmeal);

    /**
     * 删除套餐数据
     * @param id id
     */
    void delete(Long id);

    /**
     * 分页查询
     * @param queryString
     * @return
     */
    Page<TSetmeal> findPage(String queryString);

    /**
     * 根据套餐Id 查询套餐信息
     * @param id
     * @return
     */
    TSetmeal findById(Long id);

    /**
     * 根据套餐id 删除对应菜单数据
     * @param setmalId
     */
    void deleteCheckGroupBySetmalId(@Param("setmalId") Long setmalId);

    /**
     * 在套餐权限关系表中添加
     * @param setmalId
     * @param checkGroupId
     */
    void insertCheckGroup(@Param("setmalId") Long setmalId, @Param("checkGroupId") Long checkGroupId);

    /**
     * 根据套餐标签查询套餐信息
     * @param code
     * @return
     */
    TSetmeal findByCode(String code);

    /**
     * 查询所有套餐信息
     * @return
     */
    List<TSetmeal> findAll();

    /**
     * 根据套餐Id查询套餐包含检查组信息
     * @param id
     * @return
     */
    List<TCheckGroup> findGroup(@Param("setmealId") Long id);

    /**
     * 根据套餐Id查询套餐包含检查组信息
     * @param id
     * @return
     */
    List<TCheckItem> findItem(@Param("setmealId") Long id);

    /**
     * 获取套餐统计数据、套餐名称、预约数量
     */
    List<Map<String, Object>> findSetmealCount();
}
