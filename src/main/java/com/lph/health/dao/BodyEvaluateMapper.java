package com.lph.health.dao;


import com.github.pagehelper.Page;
import com.lph.health.entity.BodyEvaluate;
import com.lph.health.utils.Result;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-11-17
 */
@Repository
public interface BodyEvaluateMapper{
    
    /**
     * 分页查询预约信息
     * @param queryString
     * @return
     */
    Page<Map> findPage(String queryString);

    /**
     * 添加体制评估
     * @param bodyEvaluate
     */
    void insert(BodyEvaluate bodyEvaluate);

    /**
     * 修改体制评估
     * @param bodyEvaluate
     */
    void update(BodyEvaluate bodyEvaluate);

    /**
     * 根据评估Id查询评估
     * @param id
     * @return
     */
    BodyEvaluate findBeByBeId(@Param("value") Long id);
}
