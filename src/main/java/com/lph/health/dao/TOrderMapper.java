package com.lph.health.dao;

import com.github.pagehelper.Page;
import com.lph.health.entity.TOrder;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-11-14
 */
@Repository
public interface TOrderMapper{
    /**
     * 分页查询
     * @param queryString
     * @return
     */
    Page<TOrder> findPage(String queryString);

    /**
     * 添加订单信息
     * @param tOrder 订单信息数据
     */
    void insert(TOrder tOrder);

    /**
     * 修改订单信息
     * @param tOrder
     */
    void update(TOrder tOrder);

    /**
     * 删除订单信息数据
     * @param id id
     */
    void delete(Long id);

    /**
     * 动态查询
     * @param order
     * @return
     */
    List<TOrder> findByCondition(TOrder order);

    /**
     * 根据预约id查询预约信息，包括体检人信息、套餐信息
     * @param id
     * @return
     */
    Map findById4Detail(Long id);

    /**
     * 根据日期统计预约数
     * @param date
     * @return
     */
    Integer findOrderCountByDate(String date);

    /**
     * 根据日期统计预约数，统计指定日期之后的预约数
     * @param date
     * @return
     */
    Integer findOrderCountAfterDate(String date);

    /**
     * 根据日期统计到诊数
     * @param date
     * @return
     */
    Integer findVisitsCountByDate(String date);

    /**
     * 根据日期统计到诊数，统计指定日期之后的到诊数
     * @param date
     * @return
     */
    Integer findVisitsCountAfterDate(String date);

    /**
     * 热门套餐，查询前5条
     * @return
     */
    List<Map> findHotSetmeal();

    /**
     * 查询体检评估未完成的订单
     * @return
     */
    List<TOrder> findBdStatus();

    /**
     * 查询体质评估未完成的订单
     * @return
     */
    List<TOrder> findBoEvStatus();

    /**
     * 查询心理评估未完成的订单
     * @return
     */
    List<TOrder> findMeEvStatus();

    /**
     * 查询心理评估未完成的订单
     * @return
     */
    List<TOrder> findEvStatus();

    /**
     * 根据订单编号查询预约信息
     * @param id
     * @return
     */
    TOrder findById(@Param("orderId") Long id);

    /**
     *
     */
    void editEvStatusByOrderId(TOrder order);

}
