package com.lph.health.dao;

import com.github.pagehelper.Page;
import com.lph.health.entity.PlanFood;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-11-21
 */
@Repository
public interface PlanFoodMapper {
    /**
     * 分页查询食物方案信息
     * @param queryString
     * @return
     */
    Page<PlanFood> findPage(String queryString);

    /**
     * 根据食物方案名称查询
     * @param instructions
     * @return
     */
    PlanFood findByInstructions(String instructions);

    /**
     * 添加食物方案
     * @param planFood
     */
    void insert(PlanFood planFood);

    /**
     * 删除食物方案
     * @param id
     */
    void delete(Long id);

    /**
     * 更新食物方案
     * @param planFood
     */
    void update(PlanFood planFood);

    /**
     * 根据Id查询方案信息
     * @param id
     * @return
     */
    PlanFood findById(Long id);

    /**
     * 根据计划id查询食物方案
     * @param queryInfo
     * @return
     */
    Page<PlanFood> findByPlanId(String queryInfo);

    void insertFoods(@Param("foodId") Long foodId, @Param("planFoodId") Long planFoodId);

    void deleteFoodByPlanFoodId(@Param("planFoodId") Long planFoodId);
}
