package com.lph.health.dao;

import com.github.pagehelper.Page;
import com.lph.health.entity.BodyDate;
import com.lph.health.entity.BodyEvaluate;
import com.lph.health.utils.Result;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-11-19
 */
@Repository
public interface BodyDateMapper{

    /**
     * 分页查询预约信息
     * @param queryString
     * @return
     */
    Page<Map> findPage(String queryString);

    /**
     * 添加体检评估
     * @param bodyDate
     */
    void insert(BodyDate bodyDate);

    /**
     * 修改体检评估
     * @param bodyDate
     */
    void update(BodyDate bodyDate);

    /**
     * 根据评估Id查询评估
     * @param id
     * @return
     */
    BodyDate findBdByBdId(@Param("value") Long id);
}
