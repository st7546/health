package com.lph.health.dao;


import com.github.pagehelper.Page;
import com.lph.health.entity.TCheckGroup;
import com.lph.health.entity.TCheckItem;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-10-26
 */
@Repository
public interface TCheckGroupMapper{

    /**
     * 添加角色息
     * @param tCheckGroup 角色数据
     */
    void insert(TCheckGroup tCheckGroup);

    /**
     * 修改角色信息
     * @param tCheckGroup 权限数据
     */
    void update(TCheckGroup tCheckGroup);

    /**
     * 删除角色数据
     * @param id id
     */
    void delete(Long id);

    /**
     * 分页查询
     * @param queryString
     * @return
     */
    Page<TCheckGroup> findPage(String queryString);

    /**
     * 根据角色Id 查询角色信息
     * @param id
     * @return
     */
    TCheckGroup findById(Long id);

    /**
     * 根据检查组id查询检查项id信息
     * @param setmealId
     * @return
     */
    List<TCheckGroup> findBySetmealId(@Param("setmealId") Long setmealId);

    /**
     * 根据角色id 删除对应菜单数据
     * @param checkGroupId
     */
    void deleteCheckItemByCheckGroupId(@Param("checkGroupId") Long checkGroupId);

    /**
     * 在角色权限关系表中添加
     * @param checkItemId
     * @param checkGroupId
     */
    void insertCheckItem(@Param("checkItemId") Long checkItemId, @Param("checkGroupId") Long checkGroupId);

    /**
     * 根据角色标签查询角色信息
     * @param name
     * @return
     */
    TCheckGroup findByName(String name);

    /**
     * 查询所有检查组信息
     * @return
     */
    List<TCheckGroup> findAll();

    /**
     * 根据检查组Id查询其包含的检查项信息
     * @param id
     * @return
     */
    List<TCheckItem> findItem(@Param("checkGroupId") Long id);
}
