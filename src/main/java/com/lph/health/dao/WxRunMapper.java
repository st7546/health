package com.lph.health.dao;

import com.lph.health.entity.WxRun;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-11-27
 */
@Repository
public interface WxRunMapper{
    /**
     * 添加微信运动信息
     * @param wxRun
     */
    void insert(WxRun wxRun);

    /**
     * 更改微信运动信息
     * @param wxRun
     */
    void update(WxRun wxRun);

    /**
     * 根据用户ID以及日期
     * @return
     */
    List<WxRun> findByTime(@Param("beginTime") String beginTime, @Param("endTime") String endTime, @Param("userId") Long userId);

    /**
     * 根据日期查询
     * @param date
     * @return
     */
    WxRun findByDate(String date);
}
