package com.lph.health.dao;


import com.github.pagehelper.Page;
import com.lph.health.entity.SysMenu;
import com.lph.health.entity.SysPermission;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * <p>
 *  权限数据的增删改查
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-10-15
 */
@Repository
public interface SysPermissionMapper {

    /**
     * 添加权限信息
     * @param sysPermission 权限数据
     */
    void insert(SysPermission sysPermission);

    /**
     * 修改权限信息
     * @param sysPermission 权限数据
     */
    void update(SysPermission sysPermission);

    /**
     * 删除权限数据
     * @param id id
     */
    void delete(Long id);

    /**
     * 分页查询
     * @param queryString
     * @return
     */
    Page<SysPermission> findPage(String queryString);

    /**
     * 根据角色id查询权限信息
     * @param roleId
     * @return
     */
    List<SysPermission> findByRoleId(@Param("roleId") Long roleId);

    /**
     * 根据角色id查询权限信息
     * @return
     */
    List<SysPermission> findAll();

}
