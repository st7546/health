package com.lph.health.controller;

import com.lph.health.entity.BodyDate;
import com.lph.health.service.BodyDateService;
import com.lph.health.utils.QueryInfo;
import com.lph.health.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-11-19
 */
@RestController
@RequestMapping("/bodyDate")
@Api(tags = "体检评估")
public class BodyDateController {
    @Autowired
    private BodyDateService bodyDateService;

    @ApiOperation(value = "分页查询")
    @PostMapping("/findPage")
    public Result findPage(@RequestBody QueryInfo queryInfo){

        return Result.success("分页查询成功!", bodyDateService.findPage(queryInfo));
    }

    @ApiOperation(value = "添加体检评估")
    @PostMapping("/insert")
    public Result insert(@RequestBody BodyDate bodyDate) {
        return bodyDateService.insert(bodyDate);
    }

    @ApiOperation(value = "修改体检评估")
    @PutMapping("/update")
    public Result update(@RequestBody BodyDate bodyDate) {
        return bodyDateService.update(bodyDate);
    }

    @ApiOperation(value = "根据评估Id查询评估")
    @PostMapping("/findBdByBdId/{id}")
    public Result findBdByBdId(@PathVariable Long id){ return bodyDateService.findBdByBdId(id); }
}

