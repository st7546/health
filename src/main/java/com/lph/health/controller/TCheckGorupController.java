package com.lph.health.controller;

import com.lph.health.entity.TCheckGroup;
import com.lph.health.service.TChcekGroupService;
import com.lph.health.utils.QueryInfo;
import com.lph.health.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  角色控制层
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-10-15
 */
@RestController
@Api(tags = "检查组管理")
@RequestMapping("/checkGroup")
public class TCheckGorupController {

    @Autowired
    private TChcekGroupService tChcekGroupService;

    @ApiOperation(value = "分页查询")
    @PostMapping("/findPage")
    public Result findPage(@RequestBody QueryInfo queryInfo){
        return tChcekGroupService.findPage(queryInfo);
    }

    @ApiOperation(value = "删除检查组信息")
    @DeleteMapping("/delete/{id}")
    public Result delete(@PathVariable("id") Long id){
        return tChcekGroupService.delete(id);
    }

    @ApiOperation(value = "添加检查组信息")
    @PostMapping("/insert")
    public Result insert(@RequestBody TCheckGroup tCheckGroup){
        return tChcekGroupService.insert(tCheckGroup);
    }

    @ApiOperation(value = "修改检查组信息")
    @PutMapping("/update")
    public Result update(@RequestBody TCheckGroup tCheckGroup){
        return tChcekGroupService.update(tCheckGroup);
    }

    @ApiOperation(value = "添加检查组信息时,查询全部检查项")
    @GetMapping("/findAll")
    public Result findAll(){
        return tChcekGroupService.findAll();
    }

    @ApiOperation(value = "查询对应检查项")
    @PostMapping("/findItem/{id}")
    public Result findItem(@PathVariable("id") Long id){return tChcekGroupService.findItem(id);}
}
