package com.lph.health.controller;


import com.lph.health.service.TMemberService;
import com.lph.health.utils.Result;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-11-13
 */
@RestController
@RequestMapping("/tMember")
public class TMemberController {


    @Autowired
    private TMemberService tMemberService;

    @ApiOperation(value = "根据ID获取会员信息")
    @PostMapping("/findByMerId/{id}")
    public Result findByMerId(@PathVariable Long id){
        return tMemberService.findByMerId(id);
    }
}

