package com.lph.health.controller;

import com.lph.health.entity.TCheckItem;
import com.lph.health.service.TCheckItemService;
import com.lph.health.utils.QueryInfo;
import com.lph.health.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  权限数据控制层
 * </p>
 * @RestController是@ResponseBody和@Controller的组合注解
 * @author 墨浅清谈
 * @since 2021-10-15
 */

@Api(tags = "检查项数据")
@RestController
@RequestMapping("/checkItem")
public class TCheckItemController {

    @Autowired
    private TCheckItemService tCheckItemService;

    /**
     * RequestBody 将前端传来的json数据解析为对象数据
     */
    @ApiOperation(value = "分页查询")
    @PostMapping("/findPage")
    public Result findPage(@RequestBody QueryInfo queryInfo){
        return tCheckItemService.findPage(queryInfo);
    }

    /**
     * RequestBody 将前端传来的json数据解析为对象数据
     */
    @ApiOperation(value = "添加检查项")
    @PostMapping("/insert")
    public Result insert(@RequestBody TCheckItem tCheckItem){
        return tCheckItemService.insert(tCheckItem);
    }

    /**
     * RequestBody 将前端传来的json数据解析为对象数据
     */
    @ApiOperation(value = "修改检查项")
    @PutMapping("/update")
    public Result update(@RequestBody TCheckItem tCheckItem){
        return tCheckItemService.update(tCheckItem);
    }

    /**
     * RequestBody 将前端传来的json数据解析为对象数据
     */
    @ApiOperation(value = "删除检查项")
    @DeleteMapping("/delete/{id}")
    public Result delete(@PathVariable("id") Long id){
        return tCheckItemService.delete(id);
    }

    /**
     * 查询所有检查项数据
     * @return
     */
    @ApiOperation(value = "查询所有检查项数据")
    @GetMapping("/findAll")
    public Result findAll(){
        return tCheckItemService.findAll();
    }
}

