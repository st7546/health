package com.lph.health.controller;

import com.lph.health.entity.SysUser;
import com.lph.health.service.SysUserService;
import com.lph.health.utils.QueryInfo;
import com.lph.health.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  用户管理控制层
 * </p>
 * @RestController是@ResponseBody和@Controller的组合注解
 * @author 墨浅清谈
 * @since 2021-10-15
 */

@Api(tags = "用户管理")
@RestController
@RequestMapping("/sysUser")
public class SysUserController {

    @Autowired
    private SysUserService sysUserService;


    /**
     * RequestBody 将前端传来的json数据解析为对象数据
     */
    @ApiOperation(value = "分页查询")
    @PostMapping("/findPage")
    public Result findPage(@RequestBody QueryInfo queryInfo){
        return sysUserService.findPage(queryInfo);
    }

    /**
     * RequestBody 将前端传来的json数据解析为对象数据
     */
    @ApiOperation(value = "添加用户")
    @PostMapping("/insert")
    public Result insert(@RequestBody SysUser sysUser){
        return sysUserService.insertUser(sysUser);
    }

    /**
     * RequestBody 将前端传来的json数据解析为对象数据
     */
    @ApiOperation(value = "修改用户")
    @PutMapping("/update")
    public Result update(@RequestBody SysUser sysUser){
        return sysUserService.updateUser(sysUser);
    }

    /**
     * RequestBody 将前端传来的json数据解析为对象数据
     */
    @ApiOperation(value = "删除用户")
    @DeleteMapping("/delete/{id}")
    public Result delete(@PathVariable("id") Long id){
        return sysUserService.deleteUser(id);
    }

}

