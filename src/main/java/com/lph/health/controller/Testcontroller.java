package com.lph.health.controller;

import com.lph.health.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
//@CrossOrigin //跨域注解
@Api(tags = "测试接口")
public class Testcontroller {

    @ApiOperation(value = "测试Test")
    @PreAuthorize("hasAuthority('USER_DELETE')")
    @GetMapping("/test")
    public Result test(){
        return Result.success("信息返回成功","你好！");
    }
}
