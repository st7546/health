package com.lph.health.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lph.health.entity.WxRun;
import com.lph.health.service.WxRunService;
import com.lph.health.utils.Date2Util;
import com.lph.health.utils.DecryptUtil;
import com.lph.health.utils.Result;
import com.lph.health.utils.SecurityUtil;
import com.lph.health.vo.RunVo;
import com.lph.health.vo.StepVo;
import com.lph.health.vo.WxVo;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-11-27
 */
@RestController
@RequestMapping("/calories")
public class WxRunController {

    @Autowired
    private WxRunService wxRunService;

    @ApiOperation(value = "第一次解密获取的数据")
    @PostMapping("/decrypt")
    public Result getRunStep(@RequestBody WxVo wxVo) {
        byte[] decrypt = DecryptUtil.decrypt(Base64.decodeBase64(wxVo.getEncryptedData()), Base64.decodeBase64(wxVo.getSessionKey()), Base64.decodeBase64(wxVo.getIv()));
        if (decrypt != null && decrypt.length > 0) {
            String info = new String(decrypt, StandardCharsets.UTF_8);
            JSONObject object = JSON.parseObject(info);
            //获取微信步数
            String stepInfoList = object.getString("stepInfoList");
            List<StepVo> steps = JSON.parseArray(stepInfoList, StepVo.class);
            WxRun wxRun = new WxRun();
            wxRun.setUserId(SecurityUtil.getUserId());
            for (StepVo step : steps) {
                String stampToDate = Date2Util.stampToDate(step.getTimestamp()*1000);
                Result run = wxRunService.findByDate(stampToDate);
                WxRun data = (WxRun)run.getData();
                if (data == null) {
                    wxRun.setRunDate(stampToDate);
                    wxRun.setRunStep(step.getStep());
                    wxRunService.insert(wxRun);
                } else {
                    //不为空并且步数不等的情况
                    if (!step.getStep().equals(data.getRunStep())) {
                        wxRun.setRunDate(stampToDate);
                        wxRun.setRunStep(step.getStep());
                        wxRunService.update(wxRun);
                    }
                }
            }
        }
        //默认查询当前时间的
        return wxRunService.findRun(Date2Util.getNowDate());
    }

    @ApiOperation(value = "根据时间获取用户信息")
    @PostMapping("/getRunInfo")
    public Result getRunInfo(RunVo runVo) {
        runVo.setUserId(SecurityUtil.getUserId());
        return wxRunService.getRunInfo(runVo);
    }
}

