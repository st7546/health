package com.lph.health.controller;


import com.lph.health.entity.BodyEvaluate;
import com.lph.health.service.BodyEvaluateService;
import com.lph.health.utils.QueryInfo;
import com.lph.health.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-11-17
 */
@RestController
@RequestMapping("/bodyEvaluate")
@Api(tags = "体质评估")
public class BodyEvaluateController {

    @Autowired
    private BodyEvaluateService bodyEvaluateService;

    @ApiOperation(value = "分页查询")
    @PostMapping("/findPage")
    public Result findPage(@RequestBody QueryInfo queryInfo){

        return Result.success("分析查询成功!", bodyEvaluateService.findPage(queryInfo));
    }

    @ApiOperation(value = "添加体质评估")
    @PostMapping("/insert")
    public Result insert(@RequestBody BodyEvaluate bodyEvaluate) {
        return bodyEvaluateService.insert(bodyEvaluate);
    }

    @ApiOperation(value = "修改体质评估")
    @PutMapping("/update")
    public Result update(@RequestBody BodyEvaluate bodyEvaluate) {
        return bodyEvaluateService.update(bodyEvaluate);
    }

    @ApiOperation(value = "查询所有健康员")
    @PostMapping("/findAssessor")
    public Result findAssessor(){
        return bodyEvaluateService.findAssessor();
    }

    @ApiOperation(value = "根据评估Id查询评估")
    @PostMapping("/findBeByBeId/{id}")
    public Result findBeByBeId(@PathVariable Long id){ return bodyEvaluateService.findBeByBeId(id); }
}

