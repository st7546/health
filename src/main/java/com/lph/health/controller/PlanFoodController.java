package com.lph.health.controller;

import com.lph.health.entity.Food;
import com.lph.health.entity.PlanFood;
import com.lph.health.service.PlanFoodService;
import com.lph.health.utils.QueryInfo;
import com.lph.health.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-11-21
 */
@RestController
@RequestMapping("/planFood")
@Api(tags = "食物方案接口")
public class PlanFoodController {

    @Autowired
    private PlanFoodService planFoodService;

    @ApiOperation(value = "分页查询食物方案及其食品信息")
    @PostMapping("/findPlanFoodPage")
    public Result findPlanFoodPage(@RequestBody QueryInfo queryInfo) {
        return planFoodService.findPlanFoodPage(queryInfo);
    }

    @ApiOperation(value = "添加食物方案")
    @PostMapping("/insert")
    public Result insert(@RequestBody PlanFood planFood) {
        return planFoodService.insert(planFood);
    }

    @ApiOperation(value = "修改食物方案")
    @PostMapping("/update")
    public Result update(@RequestBody PlanFood planFood) {
        return planFoodService.update(planFood);
    }

    @ApiOperation(value = "删除食物方案")
    @DeleteMapping("/delete/{id}")
    public Result delete(@PathVariable Long id) {
        return planFoodService.delete(id);
    }

    @ApiOperation(value = "根据计划查询方案")
    @PostMapping("/planId")
    public Result findByPlanId(@RequestBody QueryInfo queryInfo) {
        return planFoodService.findByPlanId(queryInfo);
    }
}

