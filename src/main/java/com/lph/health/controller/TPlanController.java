package com.lph.health.controller;

import com.lph.health.entity.TPlan;
import com.lph.health.service.TPlanService;
import com.lph.health.utils.QueryInfo;
import com.lph.health.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-11-21
 */
@RestController
@RequestMapping("/tPlan")
@Api(tags = "方案计划接口")
public class TPlanController {

    @Autowired
    private TPlanService planService;

    @ApiOperation(value = "添加方案计划")
    @PostMapping("/insert")
    public Result insertType(@RequestBody TPlan tPlan) {
        return planService.insertTPlan(tPlan);
    }

    @ApiOperation(value = "删除方案计划")
    @DeleteMapping("/delete/{id}")
    public Result deleteType(@PathVariable Long id) {
        return planService.deleteTPlan(id);
    }

    @ApiOperation(value = "修改方案计划")
    @PostMapping("/update")
    public Result updateType(@RequestBody TPlan tPlan) {
        return planService.updateTPlan(tPlan);
    }

    @GetMapping("/planAll")
    public Result planAll() {
        return planService.planAll();
    }

    @PostMapping("/findPlanPage")
    public Result findPlanPage(@RequestBody QueryInfo queryInfo) {
        return planService.findPlanPage(queryInfo);
    }

    /**
     * 查询未分配食物方案的计划
     * @return
     */
    @GetMapping("/findFoodStatus")
    public Result findFoodStatus(){
        return planService.findFoodStatus();
    }

    @ApiOperation(value = "根据计划Id查询计划")
    @PostMapping("/findPlanById/{id}")
    public Result findPlanById(@PathVariable Long id){ return planService.findPlanById(id); }

    @ApiOperation(value = "根据预约ID查询套餐详情")
    @PostMapping("findBySetId/{id}")
    public Result findBySetId(@PathVariable("id") Long id){return planService.findBySetId(id);}

    @ApiOperation(value = "根据预约ID查询套餐详情")
    @PostMapping("findByMerId/{id}")
    public Result findByMerId(@PathVariable("id") Long id){return planService.findByMerId(id);}
}

