package com.lph.health.controller;

import com.lph.health.entity.TSetmeal;
import com.lph.health.service.TSetmealService;
import com.lph.health.utils.QueryInfo;
import com.lph.health.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  套餐控制层
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-10-15
 */
@RestController
@Api(tags = "套餐管理")
@RequestMapping("/setmeal")
public class TSetmealController {

    @Autowired
    private TSetmealService tSetmealService;

    @ApiOperation(value = "分页查询")
    @PostMapping("/findPage")
    public Result findPage(@RequestBody QueryInfo queryInfo){
        return tSetmealService.findPage(queryInfo);
    }

    @ApiOperation(value = "删除检查组信息")
    @DeleteMapping("delete/{id}")
    public Result delete(@PathVariable("id") Long id){
        return tSetmealService.delete(id);
    }

    @ApiOperation(value = "添加检查组信息")
    @PostMapping("/insert")
    public Result insert(@RequestBody TSetmeal tSetmeal){
        return tSetmealService.insert(tSetmeal);
    }

    @ApiOperation(value = "修改检查组信息")
    @PutMapping("/update")
    public Result update(@RequestBody TSetmeal tSetmeal){
        return tSetmealService.update(tSetmeal);
    }

    @ApiOperation(value = "添加套餐信息时,查询全部检查组")
    @GetMapping("/findAll")
    public Result findAll(){
        return tSetmealService.findAll();
    }

    @ApiOperation(value = "查询对应检查项")
    @PostMapping("/findGroup/{id}")
    public Result findGroup(@PathVariable("id") Long id){return tSetmealService.findGroup(id);}

    /**
     * 移动端请求接口
     * 套餐详情包含 套餐基本信息、套餐对应检查组信息、检查组对应检查项信息
     * @param id
     * @return
     */
    @ApiOperation(value = "根据套餐ID查询套餐详情")
    @PostMapping("findBySetId/{id}")
    public Result findById(@PathVariable("id") Long id){return tSetmealService.findById(id);}

}
