package com.lph.health.controller;


import com.lph.health.service.TOrderService;
import com.lph.health.utils.MailUtil;
import com.lph.health.utils.RedisUtil;
import com.lph.health.utils.Result;
import com.lph.health.vo.MailVo;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-11-14
 */
@RestController
@RequestMapping("/order")
@Api(tags = "预约业务")
@Slf4j
public class TOrderController {
    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private TOrderService tOrderService;

    @Autowired
    private MailUtil mailUtil;

    // 线下体检预约
    @PostMapping("/submit")
    public Result submit(@RequestBody Map map) throws Exception {
        String mail = (String) map.get("mail");
        // 获取缓存的验证码
        Object code = redisUtil.getValue(mail + "code");
        if(code == null){
            return Result.failure("验证码已过期");
        }
        String validateCode = (String) map.get("validateCode");
        // 比对验证码，成功后请求预约业务服务
        if(code.equals(validateCode)){
            return Result.failure("验证码不正确！");
        }
        map.put("orderType","线下预约");// 设置预约类型: 线下预约、微信预约
        Result result = tOrderService.order(map);
        // 预约成功向用户邮箱发送预约成功
        if(result.isFlag()){
            MailVo mailVo = new MailVo();
            mailVo.setSubject("健康计划定制服务平台");
            String orderDate = (String) map.get("orderDate");
            mailVo.setContent("你的体检预约成功! 时间为: ["+ orderDate+ "]");
            String[] receivers = new String[1];
            receivers[0] = mail;
            mailVo.setReceivers(receivers);
            mailUtil.sendMail(mailVo);
        }
        return result;
    }

    // 微信体检预约
    @PostMapping("/wxSubmit")
    public Result wxSubmit(@RequestBody Map map) throws Exception {
        log.info("map{}"+map);
        String email = (String) map.get("email");
        log.info("email"+email);
        // 获取缓存的验证码
        Object code = redisUtil.getValue(email + "mailCode");
        log.info("code"+code);
        if(code == null){
            return Result.failure("验证码已过期");
        }
        String validateCode = (String) map.get("msgCode");
        log.info("validateCode"+validateCode);
        // 比对验证码，成功后请求预约业务服务
        if(code.equals(validateCode)){
            return Result.failure("验证码不正确！");
        }
        map.put("orderType","微信预约");// 设置预约类型: 线下预约、微信预约
        Result result = tOrderService.order(map);
        // 预约成功向用户邮箱发送预约成功
        if(result.isFlag()){
            MailVo mailVo = new MailVo();
            mailVo.setSubject("健康计划定制服务平台");
            String orderDate = (String) map.get("orderDate");
            mailVo.setContent("你的体检预约成功! 时间为: ["+ orderDate+ "]");
            String[] receivers = new String[1];
            receivers[0] = email;
            mailVo.setReceivers(receivers);
            mailUtil.sendMail(mailVo);
        }
        return result;
    }


    /**
     * 根据预约Id查询预约信息，套餐信息，和会员信息
     * @param id
     * @return
     */
    @PostMapping("/findById/{id}")
    public Result findById(@PathVariable Long id){
        try {
            Map map = tOrderService.findById(id);
            return Result.success("查询预约信息成功!",map);
        } catch (Exception e) {
            return Result.failure("查询预约信息失败");
        }
    }

    /**
     * 查询体检评估未完成的订单
     * @return
     */
    @PostMapping("/findBdStatus")
    public Result findBdStatus(){
        return tOrderService.findBdStatus();
    }

    /**
     * 查询体质评估未完成的订单
     * @return
     */
    @PostMapping("/findBoEvStatus")
    public Result findBoEvStatus(){
        return tOrderService.findBoEvStatus();
    }

    /**
     * 查询心理评估未完成的订单
     * @return
     */
    @PostMapping("/findMeEvStatus")
    public Result findMeEvStatus(){
        return tOrderService.findMeEvStatus();
    }

    /**
     * 查询所有评估成功的订单
     * @return
     */
    @PostMapping("/findEvStatus")
    public Result findEvStatus(){
        return tOrderService.findEvStatus();
    }

    /**
     * 根据预约Id查询预约信息，套餐信息，和会员信息
     * @return
     */
    @PostMapping("/findBodyEvaById/{id}")
    public Result findBodyEvaById(@PathVariable Long id){
        return tOrderService.findBodyEvaById(id);
    }

}

