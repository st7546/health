package com.lph.health.controller;

import com.lph.health.entity.MentalEvaluate;
import com.lph.health.service.MentalEvaluateService;
import com.lph.health.utils.QueryInfo;
import com.lph.health.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-11-19
 */
@RestController
@RequestMapping("/mentalEvaluate")
@Api(tags = "心理评估")
public class MentalEvaluateController {

    @Autowired
    private MentalEvaluateService mentalEvaluateService;

    @ApiOperation(value = "分页查询")
    @PostMapping("/findPage")
    public Result findPage(@RequestBody QueryInfo queryInfo){

        return Result.success("分析查询成功!", mentalEvaluateService.findPage(queryInfo));
    }

    @ApiOperation(value = "添加心理评估")
    @PostMapping("/insert")
    public Result insert(@RequestBody MentalEvaluate mentalEvaluate) {
        return mentalEvaluateService.insert(mentalEvaluate);
    }

    @ApiOperation(value = "修改心理评估")
    @PutMapping("/update")
    public Result update(@RequestBody MentalEvaluate mentalEvaluate) {
        return mentalEvaluateService.update(mentalEvaluate);
    }

    @ApiOperation(value = "根据评估Id查询评估")
    @PostMapping("/findMeByMeId/{orderId}")
    public Result findMeByMeId(@PathVariable Long orderId){ return mentalEvaluateService.findMeByMeId(orderId); }
}

