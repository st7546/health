package com.lph.health.controller;

import com.lph.health.entity.SysPermission;
import com.lph.health.service.SysPermissionService;
import com.lph.health.utils.QueryInfo;
import com.lph.health.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  权限数据控制层
 * </p>
 * @RestController是@ResponseBody和@Controller的组合注解
 * @author 墨浅清谈
 * @since 2021-10-15
 */

@Api(tags = "权限数据")
@RestController
@RequestMapping("/Permission")
public class SysPermissionController {

    @Autowired
    private SysPermissionService sysPermissionService;


    /**
     * RequestBody 将前端传来的json数据解析为对象数据
     */
    @ApiOperation(value = "分页查询")
    @PostMapping("/findPage")
    public Result findPage(@RequestBody QueryInfo queryInfo){
        return sysPermissionService.findPage(queryInfo);
    }

    /**
     * RequestBody 将前端传来的json数据解析为对象数据
     */
    @ApiOperation(value = "添加权限")
    @PostMapping("/insert")
    public Result insert(@RequestBody SysPermission sysPermission){
        return sysPermissionService.insert(sysPermission);
    }

    /**
     * RequestBody 将前端传来的json数据解析为对象数据
     */
    @ApiOperation(value = "修改权限")
    @PutMapping("/update")
    public Result update(@RequestBody SysPermission sysPermission){
        return sysPermissionService.update(sysPermission);
    }

    /**
     * RequestBody 将前端传来的json数据解析为对象数据
     */
    @ApiOperation(value = "删除权限")
    @DeleteMapping("/delete/{id}")
    public Result delete(@PathVariable("id") Long id){
        return sysPermissionService.delete(id);
    }

    /**
     * 查询所有权限数据
     * @return
     */
    @ApiOperation(value = "查询所有权限数据")
    @GetMapping("/findAll")
    public Result findAll(){
        return sysPermissionService.findAll();
    }
}

