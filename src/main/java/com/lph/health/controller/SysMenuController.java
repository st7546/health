package com.lph.health.controller;

import com.lph.health.entity.SysMenu;
import com.lph.health.service.SysMenuService;
import com.lph.health.utils.QueryInfo;
import com.lph.health.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  菜单控制层
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-10-15
 */

@Api(tags = "菜单管理")
@RestController
@RequestMapping("/menu")
public class SysMenuController {

    @Autowired
    private SysMenuService sysMenuService;


    /**
     * RequestBody 将前端传来的json数据解析为对象数据
     */
    @ApiOperation(value = "分页查询")
    @PostMapping("/findPage")
    public Result findPage(@RequestBody QueryInfo queryInfo){
        return sysMenuService.findPage(queryInfo);
    }

    /**
     * RequestBody 将前端传来的json数据解析为对象数据
     */
    @ApiOperation(value = "添加菜单")
    @PostMapping("/insert")
    public Result insert(@RequestBody SysMenu sysMenu){
        return sysMenuService.insert(sysMenu);
    }

    /**
     * RequestBody 将前端传来的json数据解析为对象数据
     */
    @ApiOperation(value = "修改菜单")
    @PutMapping("/update")
    public Result update(@RequestBody SysMenu sysMenu){
        return sysMenuService.update(sysMenu);
    }

    /**
     * RequestBody 将前端传来的json数据解析为对象数据
     */
    @ApiOperation(value = "删除菜单")
    @DeleteMapping("/delete/{id}")
    public Result delete(@PathVariable("id") Long id){
        return sysMenuService.delete(id);
    }


    @ApiOperation(value = "查询所有的父级菜单")
    @GetMapping("/findParent")
    public Result findParent(){
        return sysMenuService.findParent();
    }
}

