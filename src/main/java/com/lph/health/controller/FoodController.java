package com.lph.health.controller;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;

import com.lph.health.entity.Food;
import com.lph.health.entity.FoodType;
import com.lph.health.service.FoodService;
import com.lph.health.utils.QiNiuUtil;
import com.lph.health.utils.QueryInfo;
import com.lph.health.utils.Result;
import com.lph.health.utils.StringUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.UUID;

/**
 * 食品库
 */
@RestController
@RequestMapping("/food")
@Api(tags = "食品库")
public class FoodController {

    @Autowired
    private FoodService foodService;

    @Autowired
    private QiNiuUtil qiNiuUtil;

    @ApiOperation(value = "通过excel批量导入")
    @PostMapping("/batchImport")
    public Result batchImport(@RequestParam("file") MultipartFile file) {
        try {
            // 创建模板
            ImportParams params = new ImportParams();
            List<Food> list = ExcelImportUtil.importExcel(file.getInputStream(), Food.class, params);
            list.forEach(item -> {
                // 如果图片不为空
                if (StringUtil.isNotEmpty(item.getImageUrls())) {
                    try {
                        // 将图片转化为FileInputStream对象
                        FileInputStream inputStream = new FileInputStream(item.getImageUrls());
                        // 用uuId 拼接图片url
                        String uuid = UUID.randomUUID().toString().substring(0, 7);
                        int index = item.getImageUrls().lastIndexOf(".");
                        // 截取后缀
                        String suffix = item.getImageUrls().substring(index);
                        String fileName = uuid + suffix;
                        // 上传至文件服务器
                        String upload = qiNiuUtil.upLoad(inputStream, fileName);
                        item.setImageUrls(upload);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            });
            return foodService.batchImport(list);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.failure("食物列表导入失败！");
        }
    }

    @ApiOperation(value = "添加菜品分类")
    @PostMapping("/type/insert")
    public Result insertType(@RequestBody FoodType foodType) {
        return foodService.insertType(foodType);
    }

    @ApiOperation(value = "删除菜品分类")
    @DeleteMapping("/type/delete/{id}")
    public Result deleteType(@PathVariable Long id) {
        return foodService.deleteType(id);
    }

    @ApiOperation(value = "修改菜品分类")
    @PostMapping("/type/update")
    public Result updateType(@RequestBody FoodType foodType) {
        return foodService.updateType(foodType);
    }

    @ApiOperation(value = "分页查询菜品分类及其菜品信息")
    @PostMapping("/type/findPage")
    public Result findPage(@RequestBody QueryInfo queryInfo) {
        return foodService.findPage(queryInfo);
    }

    @GetMapping("/typeAll")
    public Result typeAll() {
        return foodService.typeAll();
    }

    @PostMapping("/findPage")
    public Result findFoodPage(@RequestBody QueryInfo queryInfo) {
        return foodService.findFoodPage(queryInfo);
    }

    @ApiOperation(value = "添加菜品")
    @PostMapping("/insert")
    public Result insert(@RequestBody Food food) {
        return foodService.insert(food);
    }

    @ApiOperation(value = "修改菜品")
    @PostMapping("/update")
    public Result update(@RequestBody Food food) {
        return foodService.update(food);
    }

    @ApiOperation(value = "删除菜品")
    @DeleteMapping("/delete/{id}")
    public Result delete(@PathVariable Long id) {
        return foodService.delete(id);
    }

    @ApiOperation(value = "根据食物类别查询食物")
    @PostMapping("/typeId")
    public Result findFoodByTypeId(@RequestBody QueryInfo queryInfo) {
        return foodService.findFoodByTypeId(queryInfo);
    }

    @ApiOperation(value = "获取分类食物")
    @PostMapping("/findByTypeId/{id}")
    public Result findByTypeId(@PathVariable Long id) {
        return foodService.findByTypeId(id);
    }
}
