package com.lph.health.controller;

import com.lph.health.entity.Sport;
import com.lph.health.service.SportService;
import com.lph.health.utils.QueryInfo;
import com.lph.health.utils.Result;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-12-11
 */
@RestController
@RequestMapping("/sport")
public class SportController {

    @Autowired
    private SportService sportService;

    @ApiOperation(value = "分页查询")
    @PostMapping("/findPage")
    public Result findPage(@RequestBody QueryInfo queryInfo){
        return sportService.findPage(queryInfo);
    }

    @ApiOperation(value = "添加运动资讯")
    @PostMapping("/insert")
    public Result insert(@RequestBody Sport sport){
        return sportService.insert(sport);
    }

    @ApiOperation(value = "修改运动资讯")
    @PutMapping("/update")
    public Result update(@RequestBody Sport sport){
        return sportService.update(sport);
    }

    @ApiOperation(value = "删除运动资讯")
    @DeleteMapping("/delete/{id}")
    public Result delete(@PathVariable("id") Long id){
        return sportService.delete(id);
    }
}

