package com.lph.health.controller;

import com.lph.health.entity.SysRole;
import com.lph.health.service.SysRoleService;
import com.lph.health.utils.QueryInfo;
import com.lph.health.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  角色控制层
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-10-15
 */
@RestController
@Api(tags = "角色管理")
@RequestMapping("/role")
public class SysRoleController {

    @Autowired
    private SysRoleService sysRoleService;

    @ApiOperation(value = "分页查询")
    @PostMapping("/findPage")
    public Result findPage(@RequestBody QueryInfo queryInfo){
        return sysRoleService.findPage(queryInfo);
    }

    @ApiOperation(value = "删除角色信息")
    @DeleteMapping("delete/{id}")
    public Result delete(@PathVariable("id") Long id){
        return sysRoleService.delete(id);
    }

    @ApiOperation(value = "添加角色信息")
    @PostMapping("/insert")
    public Result insert(@RequestBody SysRole sysRole){
        return sysRoleService.insert(sysRole);
    }

    @ApiOperation(value = "修改角色信息")
    @PutMapping("/update")
    public Result update(@RequestBody SysRole sysRole){
        return sysRoleService.update(sysRole);
    }

    @ApiOperation(value = "添加用户信息时,查询全部角色")
    @GetMapping("/findAll")
    public Result findAll(){
        return sysRoleService.findAll();
    }
}
