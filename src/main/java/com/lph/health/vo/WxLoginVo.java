package com.lph.health.vo;

import com.lph.health.entity.SysUser;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author ajie
 * @createTime 2021年06月13日 13:20:00
 */
@Data
public class WxLoginVo {
    private String code;

    @ApiModelProperty(value = "登录用户信息", dataType = "String")
    private SysUser userInfo;

    @ApiModelProperty(value = "邮箱", dataType = "String")
    private String email;

    @ApiModelProperty(value = "邮箱验证码", dataType = "String")
    private String msgCode;
}
