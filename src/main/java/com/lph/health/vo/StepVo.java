package com.lph.health.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 步数
 */
@Data
public class StepVo {
    @ApiModelProperty("步数")
    private Long step;

    @ApiModelProperty("时间戳")
    private Long timestamp;
}
