package com.lph.health.vo;

import lombok.Data;

/**
 *
 */
@Data
public class RunVo {

    private Long userId;

    private String beginTime;

    private String endTime;
}
