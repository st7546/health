package com.lph.health.vo;

import lombok.Data;

/**
 *
 */
@Data
public class WxVo {
    private String encryptedData;

    private String iv;

    private String sessionKey;

    private String beginTime;

    private String endTime;
}
