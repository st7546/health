package com.lph.health.config.security.service;

import com.lph.health.dao.SysUserMapper;
import com.lph.health.entity.SysMenu;
import com.lph.health.entity.SysUser;
import com.lph.health.entity.TMember;
import com.lph.health.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 实现UserDetailsService接口，实现自定义登录逻辑
 * 重写loadUserByUsername方法
 */
@Service
@Slf4j
public class UserDetailServiceImpl implements UserDetailsService {

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private RedisUtil redisUtil;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        // 判断缓存中是否存在用户信息  存在则直接在缓存中取，不存在则查询数据库并把数据存入缓存
        SysUser sysUser;
        if(redisUtil.hasKey("userInfo_" + userName)){
            // 缓存中存在用户信息,直接从redis中取
           sysUser = (SysUser)redisUtil.getValue("userInfo_" + userName);
           redisUtil.expire("userInfo_" + userName, 5);
        } else {
            //在mapper中自定义登录 根据用户名获取用户信息
            sysUser= sysUserMapper.findByUserName(userName);
            if(null == sysUser){
                throw new UsernameNotFoundException("用户名或密码错误！");
            }
            Boolean admin = sysUser.getAdmin();
            Boolean member = sysUser.getMember();
            //
            if (admin) {
                // 是管理员
                sysUser.setRoles(sysUserMapper.findRoles(null));
                sysUser.setPermissions(sysUserMapper.findPermissions(null));
                // 获取父级菜单
                List<SysMenu> menus = sysUserMapper.findMenus(null);
                // 加入子级菜单
                menus.forEach(item -> item.setChildren(sysUserMapper.findChildrenMenu(item.getId(), null)));
                sysUser.setMenus(menus);
            } else if (member){
                // 普通会员 则存入相关信息
                log.info("member"+member);
                log.info("sysUser.getId()"+sysUser.getId());
                TMember members = sysUserMapper.findMembers(sysUser.getId());
                log.info("members"+members);
                sysUser.setRoles(sysUserMapper.findRoles(sysUser.getId())) ;
                sysUser.setPermissions(sysUserMapper.findPermissions(sysUser.getId()));
                sysUser.setMembers(sysUserMapper.findMembers(sysUser.getId())); // 存入普通会员档案信息
                // 获取父级菜单
                List<SysMenu> menus = sysUserMapper.findMenus(sysUser.getId());
                // 加入子级菜单
                menus.forEach(item -> item.setChildren(sysUserMapper.findChildrenMenu(item.getId(), sysUser.getId())));
                sysUser.setMenus(menus);
            } else {
                // 非管理员、非普通会员 则存入相关信息
                sysUser.setRoles(sysUserMapper.findRoles(sysUser.getId())) ;
                sysUser.setPermissions(sysUserMapper.findPermissions(sysUser.getId()));
                // 获取父级菜单
                List<SysMenu> menus = sysUserMapper.findMenus(sysUser.getId());
                // 加入子级菜单
                menus.forEach(item -> item.setChildren(sysUserMapper.findChildrenMenu(item.getId(), sysUser.getId())));
                sysUser.setMenus(menus);
            }
            redisUtil.setValueTime("userInfo_" + userName, sysUser, 5);
        }
        return sysUser;
    }
}
