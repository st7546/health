package com.lph.health.config.security.contents;

/**
 * 白名单
 */

public class SecurityContent {
    public static final String[] WHITE_LIST = {
            //登录接口
            "/user/login",

            // swagger 相关
            "/swagger-ui.html",
            "/doc.html",
            "/webjars/**",
            "/swagger-resources/**",
            "/v2/**",
            "/report/exportBusinessReport",
            "/configuration/ui",
            "/configuration/security",
            "/favicon.ico",
            "/tool/forget/password",
            "/tool/mailCode",
            "/user//mail/login",

            //微信小程序资源
            "/user/wxlogin",
            //发送邮箱验证码
            "/user/sendMail",
            //获取验证码
            "/captcha/**",
            "/getCaptchaOpen",
    };
}
