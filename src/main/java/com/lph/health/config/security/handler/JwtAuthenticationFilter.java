package com.lph.health.config.security.handler;

import com.lph.health.config.security.service.UserDetailServiceImpl;
import com.lph.health.utils.TokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * token 认证
 * 在接口访问前进行过滤
 */
@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    @Autowired
    private TokenUtil tokenUtil;

    @Autowired
    private UserDetailServiceImpl userDetailsService;

    @Value("${jwt.tokenHeader}")
    private String tokenHeader;

    @Value("${jwt.tokenHead}")
    private String tokenHead;

    /**
     * 请求前获取请求头信息 token
     * @param request
     * @param response
     * @param chain
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        //1. 获取 token
        String header = request.getHeader(tokenHeader);
        //2. 判断 token 是否存在
        if(null != header && header.startsWith(tokenHead)){
            // 拿到 token 主体信息
            String token = header.substring(tokenHead.length());
            //根据 token 获取用户名
            String userName = tokenUtil.getUserNameByToken(token);
            //3. token 存在但未登录 没有登录信息
            if(null != userName && null == SecurityContextHolder.getContext().getAuthentication()){
                // 没有登录信息，在security中存入登录信息
                UserDetails userDetails = userDetailsService.loadUserByUsername(userName);
                // 判断token 是否有效
                if(!tokenUtil.isExpiration(token) && userName.equals(userDetails.getUsername())){
                    // 刷新security 中用户的信息
                    UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                    authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                    SecurityContextHolder.getContext().setAuthentication(authenticationToken); //往security主体写入认证信息
                }
            }
        }
        // 过滤器放行
        chain.doFilter(request, response);
    }
}
