package com.lph.health.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.util.ResourceUtils;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 跨域处理配置类
 */

@Configuration //让启动类加载配置
public class WebConfig implements WebMvcConfigurer {

//    /**
//     * 解决swagger被拦截的问题
//     * @param registry
//     */
//    @Override
//    public void addResourceHandlers(ResourceHandlerRegistry registry){
//        // 解决静态资源无法访问
//        registry.addResourceHandler("/**")
//                .addResourceLocations("classpath:/static/");
//        // 解决swagger无法访问
//        registry.addResourceHandler("/swagger-ui.html")
//                .addResourceLocations("classpath:/META-INF/resources/");
//        // 解决doc无法访问
//        registry.addResourceHandler("/doc.html")
//                .addResourceLocations("classpath:/META-INF/resources/");
//        // 解决swagger的js文件无法访问
//        registry.addResourceHandler("/webjars/**")
//                .addResourceLocations("classpath:/META-INF/resources/webjars/");
//    }



    //跨域处理方法
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry
                //允许访问的路径
                .addMapping("/**")
                //配置请求源
                .allowedOrigins("http://localhost:8899")
                //允许跨域访问的方法
                .allowedMethods("GET", "POST", "DELETE", "PUT")
                //是否允许携带参数
                .allowCredentials(true)
                //请求头
                //.allowedHeaders()
                //最大的效应时间 1小时
                .maxAge(3600);
    }
}
