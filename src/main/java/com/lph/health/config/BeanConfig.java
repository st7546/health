package com.lph.health.config;

import com.qiniu.storage.Region;
import com.qiniu.storage.UploadManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 将七牛云上传对象注入到spring容器中
 */

@Configuration
public class BeanConfig {

    @Bean
    public UploadManager uploadManager(){
        com.qiniu.storage.Configuration cfg = new com.qiniu.storage.Configuration(Region.region2());
        return new UploadManager(cfg);
    }
}
