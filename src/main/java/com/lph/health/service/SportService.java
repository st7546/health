package com.lph.health.service;

import com.lph.health.entity.Sport;
import com.lph.health.entity.SysPermission;
import com.lph.health.utils.QueryInfo;
import com.lph.health.utils.Result;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-12-11
 */
public interface SportService{

    /**
     * 分页查询
     * @param queryInfo 页码 页数大小 查询内容
     * @return
     */
    Result findPage(QueryInfo queryInfo);

    /**
     * 添加运动资讯信息
     * @param sport
     * @return
     */
    Result insert(Sport sport);

    /**
     * 删除运动资讯数据
     * @param id
     * @return
     */
    Result delete(Long id);

    /**
     * 修改运动资讯数据
     * @param sport
     * @return
     */
    Result update(Sport sport);
}
