package com.lph.health.service;

import com.lph.health.entity.TSetmeal;
import com.lph.health.utils.QueryInfo;
import com.lph.health.utils.Result;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  角色服务类
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-10-15
 */
public interface TSetmealService {

    /**
     * 分页查询
     * @param queryInfo 页码 页数大小 查询内容
     * @return
     */
    Result findPage(QueryInfo queryInfo);

    /**
     * 添加角色信息
     * @param tSetmeal
     * @return
     */
    Result insert(TSetmeal tSetmeal);

    /**
     * 删除角色数据
     * @param id
     * @return
     */
    Result delete(Long id);

    /**
     * 修改角色数据
     * @param tSetmeal
     * @return
     */
    Result update(TSetmeal tSetmeal);

    /**
     * 查询所有套餐数据
     * @return
     */
    Result findAll();

    /**
     * 根据套餐id查询检查组信息
     * @param id
     * @return
     */
    Result findGroup(@Param("setmealId") Long id);

    /**
     * 根据套餐id查询检查项信息
     * @param id
     * @return
     */
    Result findItem(@Param("setmealId") Long id);


    /**
     * 根据套餐ID查询套餐信息
     * @param id
     * @return
     */
    Result findById(@Param("setmealId") Long id);

    /**
     * 查询预约套餐占比信息(套餐名称，预约个数)
     * @return
     */
    List<Map<String, Object>> findSetmealCount();
}
