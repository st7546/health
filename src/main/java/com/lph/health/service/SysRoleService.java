package com.lph.health.service;

import com.lph.health.entity.SysMenu;
import com.lph.health.entity.SysRole;
import com.lph.health.utils.QueryInfo;
import com.lph.health.utils.Result;

/**
 * <p>
 *  角色服务类
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-10-15
 */
public interface SysRoleService {

    /**
     * 分页查询
     * @param queryInfo 页码 页数大小 查询内容
     * @return
     */
    Result findPage(QueryInfo queryInfo);

    /**
     * 添加角色信息
     * @param sysRole
     * @return
     */
    Result insert(SysRole sysRole);

    /**
     * 删除角色数据
     * @param id
     * @return
     */
    Result delete(Long id);

    /**
     * 修改角色数据
     * @param sysRole
     * @return
     */
    Result update(SysRole sysRole);

    /**
     * 查询所有角色数据
     * @return
     */
    Result findAll();
}
