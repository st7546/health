package com.lph.health.service;

import com.lph.health.entity.TOrderSetting;
import com.lph.health.utils.Result;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  预约设置类
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-10-15
 */
public interface TOrderSettingService {

    /**
     * 新增预约设置
     * @return
     */
    Result add(List<TOrderSetting> list);

    /**
     * 根据页面年月查询预约设置信息
     * @param date
     * @return
     */
    List<Map> getOrderSettingByMonth(String date);

    /**
     * 根据日期设置预约人数
     * @param orderSetting
     */
    Result editNumberByDate(TOrderSetting orderSetting) throws Exception;
}
