package com.lph.health.service;

import com.lph.health.entity.TCheckItem;
import com.lph.health.utils.QueryInfo;
import com.lph.health.utils.Result;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-10-15
 */
public interface TCheckItemService {

    /**
     * 分页查询
     * @param queryInfo 页码 页数大小 查询内容
     * @return
     */
    Result findPage(QueryInfo queryInfo);

    /**
     * 添加检查项信息
     * @param tCheckItem
     * @return
     */
    Result insert(TCheckItem tCheckItem);

    /**
     * 删除检查项数据
     * @param id
     * @return
     */
    Result delete(Long id);

    /**
     * 修改检查项数据
     * @param tCheckItem
     * @return
     */
    Result update(TCheckItem tCheckItem);

    /**
     * 查询全部数据
     * @return
     */
    Result findAll();
}
