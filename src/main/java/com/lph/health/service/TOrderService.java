package com.lph.health.service;

import com.lph.health.utils.Result;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-11-14
 */
public interface TOrderService{
    /**
     * 预约业务处理
     * @param map
     * @return
     */
    Result order(Map map) throws Exception;


    /**
     * 根据预约Id查询，会员信息，套餐信息，预约日期
     * @param id
     * @return
     */
    Map findById(Long id) throws Exception;

    /**
     * 查询体检评估未完成的订单
     * @return
     */
    Result findBdStatus();

    /**
     * 查询体质评估未完成的订单
     * @return
     */
    Result findBoEvStatus();

    /**
     * 查询心理评估未完成的订单
     * @return
     */
    Result findMeEvStatus();

    /**
     * 查询心理评估未完成的订单
     * @return
     */
    Result findEvStatus();

    /**
     * 根据预约Id的预约订单
     * @return
     */
    Result findBodyEvaById(Long id);

}
