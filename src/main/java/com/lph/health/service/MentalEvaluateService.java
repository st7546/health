package com.lph.health.service;

import com.lph.health.entity.MentalEvaluate;
import com.lph.health.utils.QueryInfo;
import com.lph.health.utils.Result;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-11-19
 */
public interface MentalEvaluateService{
    /**
     * 分页查询
     * @param queryInfo 页码 页数大小 查询内容
     * @return
     */
    Result findPage(QueryInfo queryInfo);

    /**
     * 添加体质评估信息
     * @param mentalEvaluate
     * @return
     */
    Result insert(MentalEvaluate mentalEvaluate);

    /**
     * 修改体质评估信息
     * @param mentalEvaluate
     * @return
     */
    Result update(MentalEvaluate mentalEvaluate);

    /**
     * 根据评估Id查询评估信息
     */
    Result findMeByMeId(Long orderId);
}
