package com.lph.health.service;


import com.lph.health.entity.BodyEvaluate;
import com.lph.health.entity.TCheckItem;
import com.lph.health.utils.QueryInfo;
import com.lph.health.utils.Result;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-11-17
 */
public interface BodyEvaluateService{

    /**
     * 分页查询
     * @param queryInfo 页码 页数大小 查询内容
     * @return
     */
    Result findPage(QueryInfo queryInfo);

    /**
     * 添加体质评估信息
     * @param bodyEvaluate
     * @return
     */
    Result insert(BodyEvaluate bodyEvaluate);

    /**
     * 修改体质评估信息
     * @param bodyEvaluate
     * @return
     */
    Result update(BodyEvaluate bodyEvaluate);

    /**
     * 查询所有角色为健康员的用户名
     * @return
     */
    Result findAssessor();

    /**
     * 根据评估Id查询评估信息
     */
    Result findBeByBeId(Long id);
}
