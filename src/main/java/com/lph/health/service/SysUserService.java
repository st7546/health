package com.lph.health.service;

import com.lph.health.entity.SysUser;
import com.lph.health.utils.QueryInfo;
import com.lph.health.utils.Result;
import com.lph.health.vo.LoginVo;
import com.lph.health.vo.WxLoginVo;

/**
 * 用户操作逻辑接口
 */
public interface SysUserService {

    /**
     * 登录接口
     * @param loginVo  登录参数： 账号、密码
     * @return  返回 token  用token获取资源
     */
    Result login(LoginVo loginVo);

    /**
     * 根据用户民获取用户信息
     * @param userName
     * @return
     */
    SysUser findUserName(String userName);

    /**
     * 分页查询
     * @param queryInfo 页码 页数大小 查询内容
     * @return
     */
    Result findPage(QueryInfo queryInfo);

    /**
     * 添加用户信息
     * @param sysUser
     * @return
     */
    Result insertUser(SysUser sysUser);

    /**
     * 修改用户数据
     * @param sysUser
     * @return
     */
    Result updateUser(SysUser sysUser);

    /**
     * 修改用户数据
     * @param id
     * @return
     */
    Result deleteUser(Long id);


    /**
     * 根据邮箱修改密码，
     * @param email
     * @param password
     */
    void updatePwdByMail(String email, String password);

    void insertUserMember(Long memberNewId);

    /**
     * 微信登录
     * @param wxLoginVo
     * @return
     */
    Result wxlogin(WxLoginVo wxLoginVo);

    /**
     * 微信小程序运动登录接口
     * @param code
     * @return
     */
    Result runLogin(String code);

    /**
     * 获取用户信息
     * @param email
     * @return
     */
    Result getUserInfo(String email);
}
