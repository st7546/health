package com.lph.health.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lph.health.dao.BodyDateMapper;
import com.lph.health.dao.BodyEvaluateMapper;
import com.lph.health.dao.SysUserMapper;
import com.lph.health.dao.TOrderMapper;
import com.lph.health.entity.BodyDate;
import com.lph.health.entity.TOrder;
import com.lph.health.service.BodyDateService;
import com.lph.health.utils.PageResult;
import com.lph.health.utils.QueryInfo;
import com.lph.health.utils.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-11-19
 */
@Service
@Slf4j
public class BodyDateServiceImpl implements BodyDateService {

    @Autowired
    private BodyDateMapper bodyDateMapper;

    @Autowired
    private TOrderMapper orderMapper;

    @Override
    public Result findPage(QueryInfo queryInfo) {
        log.info("开始权限数据分页--->页码{}, ---{}页数, --->{}查询内容", queryInfo.getPageNumber(), queryInfo.getPageSize(), queryInfo.getQueryString());
        PageHelper.startPage(queryInfo.getPageNumber(), queryInfo.getPageSize());
        Page<Map> page = bodyDateMapper.findPage(queryInfo.getQueryString());
        long total = page.getTotal();
        log.info("查询的总条数-->{}", total);
        List<Map> result = page.getResult();
        log.info("分页列表-->{}", result);
        return PageResult.pageResult(total, result);
    }

    @Override
    public Result insert(BodyDate bodyDate) {
        TOrder order = orderMapper.findById(bodyDate.getOrderId());
        log.info("改变预约订单到诊状态、评估状态");
        if(order.getBdStatus().equals("已完成")&&order.getBoEvStatus().equals("已完成")&&order.getMeEvStatus().equals("已完成")){
            order.setEvStatus("评估成功");
        }
        order.setOrderStatus("已到诊");
        order.setBdStatus("已完成");
        orderMapper.editEvStatusByOrderId(order);
        bodyDateMapper.insert(bodyDate);
        return Result.success("添加体检评估数据成功");
    }

    @Override
    public Result update(BodyDate bodyDate) {
        bodyDateMapper.update(bodyDate);
        return Result.success("修改体检评估数据成功");
    }

    @Override
    public Result findBdByBdId(Long id) {
        return Result.success("获取体检评估数据成功!", bodyDateMapper.findBdByBdId(id));
    }
}
