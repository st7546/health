package com.lph.health.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lph.health.dao.SportMapper;
import com.lph.health.entity.Sport;
import com.lph.health.entity.SysPermission;
import com.lph.health.service.SportService;
import com.lph.health.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-12-11
 */
@Service
@Slf4j
public class SportServiceImpl implements SportService {

    @Autowired
    private SportMapper sportMapper;

    @Override
    public Result findPage(QueryInfo queryInfo) {
        log.info("开始权限数据分页--->页码{}, ---{}页数, --->{}查询内容", queryInfo.getPageNumber(), queryInfo.getPageSize(), queryInfo.getQueryString());
        PageHelper.startPage(queryInfo.getPageNumber(), queryInfo.getPageSize());
        Page<Sport> page = sportMapper.findPage(queryInfo.getQueryString());
        long total = page.getTotal();
        log.info("查询的总条数-->{}", total);
        List<Sport> result = page.getResult();
        log.info("分页列表-->{}", result);
        return PageResult.pageResult(total, result);
    }

    @Override
    public Result insert(Sport sport) {
        log.info("查询资讯是否存在");
        Sport s = sportMapper.findByTitle(sport.getTitle());
        sport.setCreateName(SecurityUtil.getUserName());// 设置为当前登录的用户
        sport.setCreateTime(Date2Util.getNowDateTime());
        if(s == null){
            sportMapper.insert(sport);
            return Result.success("新增成功!");
        } else {
            return Result.failure("该标题已存在!");
        }
    }

    @Override
    public Result delete(Long id) {
        if(id == null) {
            return Result.failure("请传递删除的id");
        }
        sportMapper.delete(id);
        return Result.success("删除成功!");
    }

    @Override
    public Result update(Sport sport) {
        log.info("查询资讯是否存在");
        Sport s = sportMapper.findByTitle(sport.getTitle());
        sport.setUpdateName(SecurityUtil.getUserName());// 设置为当前登录的用户
        sport.setUpdateTime(Date2Util.getNowDateTime());
        if(s == null || sport.getTitle().equals(s.getTitle())){
            sportMapper.update(sport);
            return Result.success("修改成功!");
        } else {
            return Result.failure("该标题已存在!");
        }
    }
}
