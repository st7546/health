package com.lph.health.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lph.health.dao.TPlanMapper;
import com.lph.health.entity.TPlan;
import com.lph.health.service.TPlanService;
import com.lph.health.utils.PageResult;
import com.lph.health.utils.QueryInfo;
import com.lph.health.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-11-21
 */
@Service
public class TPlanServiceImpl implements TPlanService {

    @Autowired
    private TPlanMapper tPlanMapper;

    @Override
    public Result updateTPlan(TPlan tPlan) {
        tPlanMapper.update(tPlan);
        return Result.success("方案计划更新成功");
    }

    @Override
    public Result deleteTPlan(Long id) {
        tPlanMapper.delete(id);
        return Result.success("方案计划删除成功");
    }

    @Override
    public Result insertTPlan(TPlan tPlan) {
        tPlan.setFoodStatus("未分配");
        tPlanMapper.insert(tPlan);
        return Result.success("方案计划添加成功");
    }

    @Override
    public Result findPlanPage(QueryInfo queryInfo) {
        PageHelper.startPage(queryInfo.getPageNumber(), queryInfo.getPageSize());
        Page<TPlan> page = tPlanMapper.findPage(queryInfo.getQueryString());
        return Result.success("计划查询成功", new PageResult(page.getTotal(), page.getResult()));
    }

    @Override
    public Result findFoodStatus() {
        return Result.success("查询健康计划成功!", tPlanMapper.findFoodStatus());
    }

    @Override
    public Result planAll() {
        return Result.success("计划查询成功！", tPlanMapper.planAll());
    }

    @Override
    public Result findPlanById(Long id) {
        return Result.success("获取体质评估数据成功!", tPlanMapper.findPlanById(id));
    }

    @Override
    public Result findBySetId(Long id) {
        return Result.success("获取套餐信息成功!",tPlanMapper.findBySetId(id));
    }

    @Override
    public Result findByMerId(Long id) {
        return Result.success("获取套餐信息成功!",tPlanMapper.findByMerId(id));
    }
}
