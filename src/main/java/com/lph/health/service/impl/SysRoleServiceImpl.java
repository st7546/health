package com.lph.health.service.impl;

import com.baomidou.mybatisplus.extension.api.R;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lph.health.dao.SysMenuMapper;
import com.lph.health.dao.SysPermissionMapper;
import com.lph.health.dao.SysRoleMapper;
import com.lph.health.entity.SysMenu;
import com.lph.health.entity.SysPermission;
import com.lph.health.entity.SysRole;
import com.lph.health.service.SysRoleService;
import com.lph.health.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-10-15
 */

@Service
@Slf4j
public class SysRoleServiceImpl implements SysRoleService {

    @Autowired
    private SysMenuMapper sysMenuMapper;

    @Autowired
    private SysPermissionMapper sysPermissionMapper;

    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Autowired
    private RedisUtil redisUtil;

    /**
     * 分页查询
     * @param queryInfo 页码 页数大小 查询内容
     * @return
     */
    @Override
    public Result findPage(QueryInfo queryInfo) {
        log.info("开始菜单数据分页--->页码{}, ---{}页数, --->{}查询内容", queryInfo.getPageNumber(), queryInfo.getPageSize(), queryInfo.getQueryString());
        PageHelper.startPage(queryInfo.getPageNumber(), queryInfo.getPageSize());
        Page<SysRole> page = sysRoleMapper.findPage(queryInfo.getQueryString());
        long total = page.getTotal();
        log.info("查询的总条数-->{}", total);
        List<SysRole> result = page.getResult();
        result.forEach(item ->{
            // 查询角色下的父级菜单信息
            List<SysMenu> menus = sysMenuMapper.findByRoleId(item.getId());
            // 遍历父级菜单信息，获取子级角色信息
            menus.forEach(menu ->{
                List<SysMenu> children = sysMenuMapper.findByRoleIdAndParentId(menu.getId(), item.getId());
                menu.setChildren(children);
            });
            item.setSysMenus(menus);
            // 查询该角色权限信息
            List<SysPermission> permissions = sysPermissionMapper.findByRoleId(item.getId());
            item.setSysPermissions(permissions);
        });
        log.info("分页列表-->{}", result);
        return PageResult.pageResult(total, result);
    }

    /**
     * 角色数据添加
     * @param sysRole
     * @Transactional 事物注解
     * @return
     */
    @Transactional
    @Override
    public Result insert(SysRole sysRole) {
        // 先查询一次角色信息判断其是否存在
        log.info("查询角色信息是否存在");
        SysRole role = sysRoleMapper.findByLabel(sysRole.getLabel());
        if(role != null) {
            return Result.failure("该角色已经存在!");
        }
        //  需要返回一个最新的角色id
        sysRoleMapper.insert(sysRole);
        // 插入角色信息后完成
        if (sysRole.getSysPermissions().size() > 0) {
            sysRole.getSysPermissions().forEach(item ->{
                log.info("添加对应权限数据");
                sysRoleMapper.insertPermissions(sysRole.getId(), item.getId());
            });
        }
        if (sysRole.getSysMenus().size() > 0) {
            sysRole.getSysMenus().forEach(item ->{
                log.info("添加对应菜单数据");
                sysRoleMapper.insertMenus(sysRole.getId(), item.getId());
            });
        }
        redisUtil.delKey("userInfo_" + SecurityUtil.getUserName());
        return Result.success("添加角色信息成功！");
    }

    /**
     * 角色数据删除
     * @param id
     * @return
     */
    @Override
    public Result delete(Long id) {
        // 先查询一次角色信息判断其是否存在
        log.info("查询角色信息是否存在");
        SysRole role = sysRoleMapper.findById(id);
        if(role == null) {
            return Result.failure("该角色不存在!");
        }
        log.info("查询该角色信息下是否有菜单、权限");
        List<SysMenu> menus = sysMenuMapper.findByRoleId(id);
        List<SysMenu> children = new ArrayList<>();
        // 在父级菜单信息加入子级菜单信息
        menus.forEach(item ->{
            children.addAll(sysMenuMapper.findByRoleIdAndParentId(item.getId(), id));
        });

        if(menus.size() > 0 || children.size() > 0){
            return Result.failure("删除失败,该角色下拥有菜单信息,请先删除对应的菜单信息");
        }
        if(sysPermissionMapper.findByRoleId(id).size() > 0){
            return Result.failure("删除失败,该角色下拥有权限信息,请先删除对应的权限信息");
        }
        sysRoleMapper.delete(id);
        redisUtil.delKey("userInfo_" + SecurityUtil.getUserName());
        return Result.success("删除角色成功");
    }

    /**
     * 角色数据修改
     * @param sysRole
     * @return
     */
    @Transactional
    @Override
    public Result update(SysRole sysRole) {
        // 先根据标签查询一次角色信息判断其是否存在
        log.info("查询更新的角色信息是否存在");
        SysRole role = sysRoleMapper.findByLabel(sysRole.getLabel());
        if(sysRole.getLabel().equals(sysRoleMapper.findById(sysRole.getId()).getLabel()) || role == null) {
            log.info("---------------"+sysRole.getSysPermissions().size());
            if (sysRole.getSysPermissions().size() >= 0) {
                log.info("先删除对应权限数据");
                // 删除所有跟该角色相关的权限数据
                sysRoleMapper.deletePerByRoleId(sysRole.getId());
                sysRole.getSysPermissions().forEach(item ->{
                    log.info("再添加对应权限数据");
                    // 这里的roleId是前端传来的
                    sysRoleMapper.insertPermissions(sysRole.getId(), item.getId());
                });
            }
            if (sysRole.getSysMenus().size() > 0) {
                log.info("先删除对应菜单信息");
                // 删除所有跟该角色相关的菜单数据
                sysRoleMapper.deleteMenuByRoleId(sysRole.getId());
                sysRole.getSysMenus().forEach(item ->{
                    log.info("再添加对应权限数据");
                    sysRoleMapper.insertMenus(sysRole.getId(), item.getId());
                });
            }
        } else {
            return Result.failure("该角色已经存在!");
        }

        sysRoleMapper.update(sysRole);
        redisUtil.delKey("userInfo_" + SecurityUtil.getUserName());
        return Result.success("修改角色信息成功！");
    }

    @Override
    public Result findAll() {
        return Result.success("查询所有角色成功", sysRoleMapper.findAll());
    }

}
