package com.lph.health.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lph.health.dao.TCheckItemMapper;
import com.lph.health.entity.TCheckItem;
import com.lph.health.service.TCheckItemService;
import com.lph.health.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-10-15
 */

@Service
@Slf4j
public class TCheckItemServiceImpl implements TCheckItemService {

    @Autowired
    private TCheckItemMapper tCheckItemMapper;

    @Autowired
    private RedisUtil redisUtil;

    @Override
    public Result findPage(QueryInfo queryInfo) {
        log.info("开始权限数据分页--->页码{}, ---{}页数, --->{}查询内容", queryInfo.getPageNumber(), queryInfo.getPageSize(), queryInfo.getQueryString());
        PageHelper.startPage(queryInfo.getPageNumber(), queryInfo.getPageSize());
        Page<TCheckItem> page = tCheckItemMapper.findPage(queryInfo.getQueryString());
        long total = page.getTotal();
        log.info("查询的总条数-->{}", total);
        List<TCheckItem> result = page.getResult();
        log.info("分页列表-->{}", result);
        return PageResult.pageResult(total, result);
    }

    /**
     * 检查项数据添加
     * @param tCheckItem
     * @return
     */
    @Override
    public Result insert(TCheckItem tCheckItem) {
        tCheckItemMapper.insert(tCheckItem);
        redisUtil.delKey("userInfo_"+ SecurityUtil.getUserName());
        return Result.success("添加检查项数据成功");
    }

    @Override
    public Result delete(Long id) {
        tCheckItemMapper.delete(id);
        redisUtil.delKey("userInfo_"+ SecurityUtil.getUserName());
        return Result.success("删除检查项数据成功");
    }

    @Override
    public Result update(TCheckItem tCheckItem) {
        tCheckItemMapper.update(tCheckItem);
        redisUtil.delKey("userInfo_"+ SecurityUtil.getUserName());
        return Result.success("修改检查项数据成功");
    }

    @Override
    public Result findAll( ) {
        return Result.success("获取所有检查项数据成功", tCheckItemMapper.findAll());
    }
}
