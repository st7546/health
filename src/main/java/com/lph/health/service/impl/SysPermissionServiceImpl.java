package com.lph.health.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lph.health.dao.SysPermissionMapper;
import com.lph.health.entity.SysPermission;
import com.lph.health.service.SysPermissionService;
import com.lph.health.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-10-15
 */

@Service
@Slf4j
public class SysPermissionServiceImpl implements SysPermissionService {

    @Autowired
    private SysPermissionMapper sysPermissionMapper;

    @Autowired
    private RedisUtil redisUtil;

    @Override
    public Result findPage(QueryInfo queryInfo) {
        log.info("开始权限数据分页--->页码{}, ---{}页数, --->{}查询内容", queryInfo.getPageNumber(), queryInfo.getPageSize(), queryInfo.getQueryString());
        PageHelper.startPage(queryInfo.getPageNumber(), queryInfo.getPageSize());
        Page<SysPermission> page = sysPermissionMapper.findPage(queryInfo.getQueryString());
        long total = page.getTotal();
        log.info("查询的总条数-->{}", total);
        List<SysPermission> result = page.getResult();
        log.info("分页列表-->{}", result);
        return PageResult.pageResult(total, result);
    }

    /**
     * 权限数据添加
     * @param sysPermission
     * @return
     */
    @Override
    public Result insert(SysPermission sysPermission) {
        sysPermissionMapper.insert(sysPermission);
        redisUtil.delKey("userInfo_"+ SecurityUtil.getUserName());
        return Result.success("添加权限数据成功");
    }

    @Override
    public Result delete(Long id) {
        sysPermissionMapper.delete(id);
        redisUtil.delKey("userInfo_"+ SecurityUtil.getUserName());
        return Result.success("删除权限数据成功");
    }

    @Override
    public Result update(SysPermission sysPermission) {
        sysPermissionMapper.update(sysPermission);
        redisUtil.delKey("userInfo_"+ SecurityUtil.getUserName());
        return Result.success("修改权限数据成功");
    }

    @Override
    public Result findAll( ) {
        return Result.success("获取所有权限数据成功", sysPermissionMapper.findAll());
    }
}
