package com.lph.health.service.impl;

import com.lph.health.dao.TOrderSettingMapper;
import com.lph.health.entity.TOrderSetting;
import com.lph.health.service.TOrderSettingService;
import com.lph.health.utils.DateUtil;
import com.lph.health.utils.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
@Slf4j
public class TOrderSettingServiceImpl implements TOrderSettingService {

    @Autowired
    private TOrderSettingMapper tOrderSettingMapper;

    /**
     * 批量导入预约设置
     * @param list
     * @return
     */
    @Override
    @Transactional
    public Result add(List<TOrderSetting> list) {
        // 先判断是否已经设置过
        if(list != null && list.size() > 0){
            for (TOrderSetting orderSetting : list) {
                //检查此数据（日期）是否存在
                Long count = tOrderSettingMapper.findCountByOrderDate(orderSetting.getOrderDate());
                if(count > 0){
                    //已经存在，执行更新操作
                    tOrderSettingMapper.editNumberByOrderDate(orderSetting);
                }else{
                    //不存在，执行添加操作
                    tOrderSettingMapper.add(orderSetting);
                }
            }
        }
        return Result.success("批量添加预约设置成功！");
    }

    /**
     * 根据年月查询预约限制信息
     * @param date
     * @return
     */
    @Override
    public List<Map> getOrderSettingByMonth(String date) {// yyyy-MM
        String begin = date + "-1"; // 2021-11-1
        String end = date + "-31"; // 2021-11-31
        Map<String, String> map = new HashMap<>();
        map.put("begin", begin);
        map.put("end", end);
        List<TOrderSetting> list = tOrderSettingMapper.getOrderSettingByMonth(map);
        List<Map> result = new ArrayList<>();
        // 重新封装
        if (list != null && list.size() > 0) {
            list.forEach(orderSetting ->{
                Map<String, Object> m = new HashMap<>();
                m.put("date", orderSetting.getOrderDate().getDate()); // 获取日期数字
                m.put("number", orderSetting.getNumber());
                m.put("reservations", orderSetting.getReservations());
                result.add(m);
            });
        }
        return  result;
    }

    /**
     * 根据日期设置预约人数
     * @param orderSetting
     */
    @Override
    @Transactional
    public Result editNumberByDate(TOrderSetting orderSetting) throws Exception {
        // 根据日期查询是否已经进行预约设置
//        String date = new SimpleDateFormat("yyyy.MM.HH").format(orderSetting.getOrderDate());
//        log.info("-++-"+date);
//        log.info("格式化后的日期"+DateUtil.parseString2Date(date));
        Date orderDate = orderSetting.getOrderDate();
        log.info("orderDate"+orderDate);
        log.info("DateUtil.parseDate2String(orderDate)"+DateUtil.parseDate2String(orderDate));
        Long count = tOrderSettingMapper.findCountByOrderDate1(DateUtil.parseDate2String(orderDate));

        log.info("count"+count);
        if(count > 0) {
            // 当前日期已有设置，需要执行更新操作
            log.info("进行修改"+orderSetting);
            String date = DateUtil.parseDate2String(orderSetting.getOrderDate());
            tOrderSettingMapper.editNumberByOrderDate(orderSetting);
            return Result.success("更新预约设置成功");
        }else {
            log.info("进行增加");
            // 当前日期没用有设置，需要执行插入操作
            tOrderSettingMapper.add(orderSetting);
            return Result.success("新增预约设置成功");
        }
    }
}
