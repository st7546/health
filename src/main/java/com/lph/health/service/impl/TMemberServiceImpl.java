package com.lph.health.service.impl;


import com.lph.health.dao.TMemberMapper;
import com.lph.health.entity.TMember;
import com.lph.health.service.TMemberService;
import com.lph.health.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-11-13
 */
@Service
public class TMemberServiceImpl implements TMemberService {

    @Autowired
    private TMemberMapper memberMapper;
    /**
     * 根据邮箱查询会员
     * @param email
     * @return
     */
    @Override
    public TMember findByEmail(String email) {
        return memberMapper.findByEmail(email);
    }

    /**
     * 新增会员
     * @param tMember
     */
    @Override
    public void insert(TMember tMember) {

    }

    /**
     * 根据月份查询会员数量
     * @param months
     * @return
     */
    @Override
    public List<Integer> findMemberCountByMonths(List<String> months) { // 传来的数据格式 2021.11
        List<Integer> memberCount = new ArrayList<>();
        for(String month: months) {
            String date = month + ".31"; // 2021.11.31
            Integer memberCountBeforeDate = memberMapper.findMemberCountBeforeDate(date);// 查询date之前
            memberCount.add(memberCountBeforeDate);
        }
        return memberCount;
    }

    /**
     * 根据会员Iduoqu会员信息
     * @param id
     * @return
     */
    @Override
    public Result findByMerId(Long id) {
        return Result.success("获取会员信息成功!",  memberMapper.findById(id));
    }
}
