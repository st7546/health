package com.lph.health.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lph.health.dao.SysMenuMapper;
import com.lph.health.entity.SysMenu;
import com.lph.health.service.SysMenuService;
import com.lph.health.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-10-15
 */

@Service
@Slf4j
public class SysMenuServiceImpl implements SysMenuService {

    @Autowired
    private SysMenuMapper sysMenuMapper;

    @Autowired
    private RedisUtil redisUtil;

    /**
     * 分页查询
     * @param queryInfo 页码 页数大小 查询内容
     * @return
     */
    @Override
    public Result findPage(QueryInfo queryInfo) {
        log.info("开始菜单数据分页--->页码{}, ---{}页数, --->{}查询内容", queryInfo.getPageNumber(), queryInfo.getPageSize(), queryInfo.getQueryString());
        PageHelper.startPage(queryInfo.getPageNumber(), queryInfo.getPageSize());
        Page<SysMenu> page = sysMenuMapper.findPage(queryInfo.getQueryString());
        long total = page.getTotal();
        log.info("查询的总条数-->{}", total);
        List<SysMenu> result = page.getResult();
        log.info("分页列表-->{}", result);
        return PageResult.pageResult(total, result);
    }

    /**
     * 菜单数据添加
     * @param sysMenu
     * @return
     */
    @Override
    public Result insert(SysMenu sysMenu) {
        sysMenuMapper.insert(sysMenu);
        redisUtil.delKey("userInfo_"+ SecurityUtil.getUserName());
        return Result.success("添加菜单数据成功");
    }

    /**
     * 菜单数据删除
     * @param id
     * @return
     */
    @Override
    public Result delete(Long id) {
        sysMenuMapper.delete(id);
        redisUtil.delKey("userInfo_"+ SecurityUtil.getUserName());
        return Result.success("删除菜单数据成功");
    }

    /**
     * 菜单数据修改
     * @param sysMenu
     * @return
     */
    @Override
    public Result update(SysMenu sysMenu) {
        sysMenuMapper.update(sysMenu);
        redisUtil.delKey("userInfo_"+ SecurityUtil.getUserName());
        return Result.success("修改菜单数据成功");
    }

    @Override
    public Result findParent() {
        return Result.success("查询父级菜单成功！", sysMenuMapper.findParent());
    }
}
