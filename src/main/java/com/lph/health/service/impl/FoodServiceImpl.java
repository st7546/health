package com.lph.health.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lph.health.dao.FoodMapper;
import com.lph.health.dao.FoodTypeMapper;
import com.lph.health.entity.Food;
import com.lph.health.entity.FoodType;
import com.lph.health.service.FoodService;
import com.lph.health.utils.PageResult;
import com.lph.health.utils.QueryInfo;
import com.lph.health.utils.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-10-15
 */

@Service
@Slf4j
public class FoodServiceImpl implements FoodService {

    @Autowired
    private FoodTypeMapper foodTypeMapper;

    @Autowired
    private FoodMapper foodMapper;

    @Override
    public Result delete(Long id) {
        foodMapper.delete(id);
        return Result.success("食物删除成功");
    }

    @Override
    public Result update(Food food) {
        foodMapper.update(food);
        return Result.success("食物修改成功");
    }

    @Override
    public Result insert(Food food) {
        foodMapper.insert(food);
        return Result.success("食物添加成功");
    }

    @Override
    public Result findPage(QueryInfo queryInfo) {
        PageHelper.startPage(queryInfo.getPageNumber(), queryInfo.getPageSize());
        Page<FoodType> page = foodTypeMapper.findPage(queryInfo.getQueryString());
        return Result.success("食物分类查询成功", new PageResult(page.getTotal(), page.getResult()));
    }

    @Override
    public Result findFoodPage(QueryInfo queryInfo) {
        PageHelper.startPage(queryInfo.getPageNumber(), queryInfo.getPageSize());
        Page<Food> page = foodMapper.findPage(queryInfo.getQueryString());
        return Result.success("食物查询成功", new PageResult(page.getTotal(), page.getResult()));
    }

    @Override
    public Result updateType(FoodType foodType) {
        foodTypeMapper.update(foodType);
        return Result.success("食物分类更新成功");
    }

    @Override
    public Result deleteType(Long id) {
        foodTypeMapper.delete(id);
        return Result.success("食物分类删除成功");
    }

    @Override
    public Result insertType(FoodType foodType) {
        foodTypeMapper.insert(foodType);
        return Result.success("食物分类添加成功");
    }

    /**
     * 批量导入
     * @param list
     * @return
     */
    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public Result batchImport(List<Food> list) {
        List<Food> foods = new ArrayList<>();
        //1. 读取分类
        for (Food food : list) {
            FoodType foodType = foodTypeMapper.findByTitle(food.getTypeTitle());
            Food title = foodMapper.findByTitle(food.getTitle());
            if (null != foodType) {
                //修改
                if (title != null) {
                    foodMapper.update(food);
                } else {
                    //添加
                    food.setTypeId(foodType.getId());
                    foods.add(food);
                }
            }
        }
        foodMapper.insertList(foods);
        return Result.success("批量导入成功！");
    }

    @Override
    public Result typeAll() {
        return Result.success("分类查询成功！", foodTypeMapper.typeAll());
    }

    @Override
    public Result findFoodByTypeId(QueryInfo queryInfo) {
        PageHelper.startPage(queryInfo.getPageNumber(), queryInfo.getPageSize());
        String queryString = queryInfo.getQueryString();
        Page<Food> foods = foodMapper.findByTypeId(queryString);
        return Result.success("食物查询成功！", new PageResult(foods.getTotal(), foods.getResult()));
    }

    /**
     * 获取分类食物
     * @param id
     * @return
     */
    public Result findByTypeId(Long id){ return Result.success("分类查询成功!", foodMapper.findFoodByTypeId(id)); }
}
