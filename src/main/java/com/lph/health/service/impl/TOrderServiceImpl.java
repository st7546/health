package com.lph.health.service.impl;

import com.lph.health.dao.TMemberMapper;
import com.lph.health.dao.TOrderMapper;
import com.lph.health.dao.TOrderSettingMapper;
import com.lph.health.entity.TMember;
import com.lph.health.entity.TOrder;
import com.lph.health.entity.TOrderSetting;
import com.lph.health.service.SysUserService;
import com.lph.health.service.TOrderService;
import com.lph.health.utils.DateUtil;
import com.lph.health.utils.MakeOrderNumUtil;
import com.lph.health.utils.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * <p>
 *  服务实现类
 *  预约时先检查选择的预约时间是否是被设置的，否则不能进行预约
 *  检查预约人数是否满了
 *  判断预约是否重复，同一个用户一天只能进行一次预约
 *  检查当前用户是否是会员，如果是会员直接进行预约，不是则自动完成注册并预约
 *  预约完成后更新预约时间
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-11-14
 */
@Service
@Slf4j
public class TOrderServiceImpl implements TOrderService {

    @Autowired
    private TOrderMapper orderMapper;

    @Autowired
    private TMemberMapper memberMapper;

    @Autowired
    private TOrderSettingMapper tOrderSettingMapper;

    Long memberNewId = null;

    @Autowired
    private SysUserService sysUserService;

    /**
     * 预约业务
     * @param map
     * @return
     */
    @Override
    @Transactional
    public Result order(Map map) throws Exception{
        log.info("mapService{}"+map);
        // 预约时先检查选择的预约时间是否是被设置的，否则不能进行预约
        String orderDate = (String) map.get("orderDate");
        TOrderSetting orderSetting = tOrderSettingMapper.findByOrderDate(DateUtil.parseString2Date(orderDate));
        if (orderSetting == null) {
            // 指定日期没有进行预约设置，无法进行预约
            return Result.failure("所选日期不能进行预约");
        }
        // 检查预约人数是否满了
        Integer number = orderSetting.getNumber();// 可预约人数
        Integer reservations = orderSetting.getReservations();// 已预约人数
        if(reservations>=number){
            // 已经约满，无法进行预约
            return Result.failure("当天预约人数已满");
        }
        // 判断预约是否重复，同一个用户一天只能进行一次预约
        String mail = (String) map.get("email");
        log.info("mail"+mail);
        TMember member = memberMapper.findByEmail(mail);
        if(member != null){
            // 判断是否在重复预约
            Long memberId = member.getId();// 会员ID
            Date order_Date = DateUtil.parseString2Date(orderDate);// 预约日期
            String setmealId = (String) map.get("setmealId");// 套餐Id
            log.info("setmealId"+setmealId);
            TOrder tOrder = new TOrder(memberId, order_Date, Long.parseLong(setmealId));
            // 根据封装条件进行查询
            List<TOrder> list = orderMapper.findByCondition(tOrder);
            if(list != null && list.size() > 0 ) {
                // 重复预约，无法再次预约
                return Result.failure("已经完成预约不能重复预约");
            }
        } else {
            // 检查当前用户是否是会员，如果是会员直接进行预约，不是则自动完成注册会员档案并预约
            member = new TMember();
            member.setEmail(mail);
            member.setName((String) map.get("name"));
            member.setIdCard((String) map.get("idCard"));
            member.setSex((String) map.get("sex"));
            member.setRegTime(new Date());
            member.setFileNumber(MakeOrderNumUtil.getOrderId());
            // 判断用户档案是否存在相同身份证号码的用户
            String idCard = (String) map.get("idCard");
            TMember member2 = memberMapper.findByIdCard(idCard);
            log.info("member2"+member2);
            log.info("member"+member);
            if ( member2 == null ) {
                // 完成注册，后期详细信息由管理员线下档案完善信息
                memberMapper.insert(member);
                memberNewId = member.getId(); // 获取新增档案Id
                sysUserService.insertUserMember(memberNewId); // 新增关系
            } else {
                return Result.failure("该身份证号已被注册!");
            }
        }
        // 保存预约信息到预约表、更新预约时间
        log.info("保存预约");
        TOrder order = new TOrder();
        order.setMemberId(member.getId());// 需要返回自动生成的主键值,设置会员Id
        order.setOrderDate(DateUtil.parseString2Date(orderDate));// 设置预约日期
        order.setOrderType((String) map.get("orderType"));// 预约类型
        order.setOrderStatus("未到诊");// 到诊状态
        order.setBoEvStatus("未评估");// 体质评估
        order.setMeEvStatus("未评估");// 心理评估
        order.setBdStatus("未评估");// 体检评估
        order.setEvStatus("评估待完成");//评估状态
        log.info("上");
        String setmealId2 = (String) map.get("setmealId");
        order.setSetmealId(Long.parseLong(setmealId2));// 套餐Id
        log.info("下");
        orderMapper.insert(order);
        orderSetting.setReservations(orderSetting.getReservations()+1);
        tOrderSettingMapper.editReservationsByOrderDate(orderSetting);// 更新已预约人数
        return Result.success("预约成功!",order);
    }

    /**
     * 根据预约Id, 查询预约信息，会员信息，套餐信息(体检人姓名，预约日期，套餐名称，预约类型)
     * @param id
     * @return
     */
    @Override
    public Map findById(Long id) throws Exception {
        Map map = orderMapper.findById4Detail(id);
        if (map != null){
            // 处理日期格式
            Date orderDate = (Date) map.get("orderDate");
            map.put("orderDate",DateUtil.parseDate2String(orderDate));
        }
        return map;
    }

    @Override
    public Result findBdStatus() {
        return Result.success("查询预约订单成功!", orderMapper.findBdStatus());
    }

    @Override
    public Result findBoEvStatus() {
        return Result.success("查询预约订单成功!", orderMapper.findBoEvStatus());
    }

    @Override
    public Result findMeEvStatus() {
        return Result.success("查询预约订单成功!", orderMapper.findMeEvStatus());
    }

    @Override
    public Result findEvStatus() {
        return Result.success("查询预约订单成功!", orderMapper.findEvStatus());
    }

    /**
     * 根据单号查询预约信息
     * @param id
     * @return
     */
    @Override
    public Result findBodyEvaById(Long id) {
        return Result.success("查询预约订单成功!", orderMapper.findById(id));
    }

}
