package com.lph.health.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lph.health.dao.*;
import com.lph.health.entity.*;
import com.lph.health.service.SysRoleService;
import com.lph.health.service.TChcekGroupService;
import com.lph.health.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-10-15
 */

@Service
@Slf4j
public class TCheckGroupServiceImpl implements TChcekGroupService {

    @Autowired
    private TCheckItemMapper tCheckItemMapper;

    @Autowired
    private TCheckGroupMapper tCheckGroupMapper;

    @Autowired
    private RedisUtil redisUtil;

    /**
     * 分页查询
     * @param queryInfo 页码 页数大小 查询内容
     * @return
     */
    @Override
    public Result findPage(QueryInfo queryInfo) {
        log.info("开始菜单数据分页--->页码{}, ---{}页数, --->{}查询内容", queryInfo.getPageNumber(), queryInfo.getPageSize(), queryInfo.getQueryString());
        PageHelper.startPage(queryInfo.getPageNumber(), queryInfo.getPageSize());
        Page<TCheckGroup> page = tCheckGroupMapper.findPage(queryInfo.getQueryString());
        long total = page.getTotal();
        log.info("查询的总条数-->{}", total);
        List<TCheckGroup> result = page.getResult();
        result.forEach(item ->{
            // 查询检查组下的检查项信息
            List<TCheckItem> tCheckItems = tCheckItemMapper.findByCheckItemId(item.getId());
            item.setCheckItems(tCheckItems);
        });
        log.info("分页列表-->{}", result);
        return PageResult.pageResult(total, result);
    }

    /**
     * 角色数据添加
     * @param tCheckGroup
     * @Transactional 事物注解
     * @return
     */
    @Transactional
    @Override
    public Result insert(TCheckGroup tCheckGroup) {
        // 先查询一次角色信息判断其是否存在
        log.info("查询检查组信息是否存在");
        TCheckGroup checkGroup = tCheckGroupMapper.findByName(tCheckGroup.getName());
        if(checkGroup != null) {
            return Result.failure("该检查组已经存在!");
        }
        //  需要返回一个最新的检查组id
        tCheckGroupMapper.insert(tCheckGroup);
        // 插入检查组信息后完成
        if (tCheckGroup.getCheckItems().size() > 0) {
            tCheckGroup.getCheckItems().forEach(item ->{
                log.info("添加对应检查组数据");
                tCheckGroupMapper.insertCheckItem(tCheckGroup.getId(), item.getId());
            });
        }
        redisUtil.delKey("userInfo_" + SecurityUtil.getUserName());
        return Result.success("添加检查组信息成功！");
    }

    /**
     * 角色数据删除
     * @param id
     * @return
     */
    @Override
    public Result delete(Long id) {
        // 先查询一次角色信息判断其是否存在
        log.info("查询检查组信息是否存在");
        TCheckGroup checkGroup = tCheckGroupMapper.findById(id);
        if(checkGroup == null) {
            return Result.failure("该检查组不存在!");
        }
        log.info("查询该检查组信息下是否有检查项");
        List<TCheckItem> checkItems = tCheckItemMapper.findByCheckItemId(id);
        if(checkItems.size() > 0) {
            return Result.failure("删除失败,该检查组下拥有检查项信息,请先删除对应的检查项信息");
        }
        tCheckGroupMapper.delete(id);
        redisUtil.delKey("userInfo_" + SecurityUtil.getUserName());
        return Result.success("删除检查组成功");
    }

    /**
     * 角色数据修改
     * @param tCheckGroup
     * @return
     */
    @Transactional
    @Override
    public Result update(TCheckGroup tCheckGroup) {
        // 先根据检查组名称查询一次检查组信息判断其是否存在
        log.info("查询更新的角色信息是否存在");
        TCheckGroup checkGroup = tCheckGroupMapper.findByName(tCheckGroup.getName());
        if(checkGroup == null || tCheckGroup.getName().equals(tCheckGroupMapper.findById(tCheckGroup.getId()).getName())) {
            if (tCheckGroup.getCheckItems().size() > 0) {
                log.info("先删除对应检查项数据");
                // 删除所有跟该角色相关的检查项数据
                tCheckGroupMapper.deleteCheckItemByCheckGroupId(tCheckGroup.getId());
                tCheckGroup.getCheckItems().forEach(item ->{
                    log.info("再添加对应检查项数据");
                    // 这里的Id是前端传来的
                    tCheckGroupMapper.insertCheckItem(tCheckGroup.getId(), item.getId());
                });
            }
        } else {
            return Result.failure("该检查组已经存在!");
        }

        tCheckGroupMapper.update(tCheckGroup);
        redisUtil.delKey("userInfo_" + SecurityUtil.getUserName());
        return Result.success("修改检查组信息成功！");
    }

    @Override
    public Result findAll() {
        return Result.success("查询所有角色成功", tCheckGroupMapper.findAll());
    }

    @Override
    public Result findItem(Long id) {
        return Result.success("查询检查项信息成功！", tCheckGroupMapper.findItem(id));
    }

}
