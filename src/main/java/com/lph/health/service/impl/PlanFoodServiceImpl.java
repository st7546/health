package com.lph.health.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lph.health.dao.PlanFoodMapper;
import com.lph.health.entity.PlanFood;
import com.lph.health.service.PlanFoodService;
import com.lph.health.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-11-21
 */
@Service
@Slf4j
public class PlanFoodServiceImpl implements PlanFoodService {

    @Autowired
    private PlanFoodMapper planFoodMapper;

    @Override
    public Result delete(Long id) {
        planFoodMapper.delete(id);
        return Result.success("食物方案删除成功");
    }

    @Transactional
    @Override
    public Result update(PlanFood planFood) {
        // 先根据标签查询一次角色信息判断其是否存在
        log.info("查询更新的信息是否存在");
        PlanFood planFoods = planFoodMapper.findByInstructions(planFood.getInstructions());
        if(planFoods.getInstructions().equals(planFoodMapper.findById(planFood.getId()).getInstructions()) || planFoods == null) {
            log.info("---------------"+planFood.getFoods().size());
            if(planFoods.getFoods().size() >= 0){
                log.info("先删除对应食物数据");
                // 删除所有跟该方案相关的食物数据
                planFoodMapper.deleteFoodByPlanFoodId(planFood.getId());
                planFood.getFoods().forEach(item ->{
                    log.info("再添加对应权限数据");
                    planFoodMapper.insertFoods(planFood.getId(), item.getId());
                });
            }
        } else {
            return Result.failure("该方案已经存在!");
        }
        planFoodMapper.update(planFood);
        return Result.success("食物方案修改成功");
    }

    @Override
    @Transactional
    public Result insert(PlanFood planFood) {
        // 先查询一下计划信息判断其是否存在
        log.info("查询计划信息是否存在");
        PlanFood planFoods = planFoodMapper.findByInstructions(planFood.getInstructions());
        if(planFoods != null) {
            return Result.failure("该食物方案已存在!");
        }
        if (planFood.getFoods().size() > 0) {
            planFood.getFoods().forEach(item ->{
                log.info("添加对应食品信息");
                planFoodMapper.insertFoods(planFood.getId(), item.getId());
            });
        }
        planFoodMapper.insert(planFood);
        return Result.success("食物方案添加成功");
    }

    @Override
    public Result findPlanFoodPage(QueryInfo queryInfo) {
        PageHelper.startPage(queryInfo.getPageNumber(), queryInfo.getPageSize());
        Page<PlanFood> page = planFoodMapper.findPage(queryInfo.getQueryString());
        return Result.success("食物方案查询成功", new PageResult(page.getTotal(), page.getResult()));
    }

    @Override
    public Result findByPlanId(QueryInfo queryInfo) {
        PageHelper.startPage(queryInfo.getPageNumber(), queryInfo.getPageSize());
        String queryString = queryInfo.getQueryString();
        Page<PlanFood> planFoods = planFoodMapper.findByPlanId(queryString);
        return Result.success("食物方案查询成功！", new PageResult(planFoods.getTotal(), planFoods.getResult()));
    }
}
