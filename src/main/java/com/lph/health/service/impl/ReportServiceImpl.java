package com.lph.health.service.impl;

import com.lph.health.dao.TMemberMapper;
import com.lph.health.dao.TOrderMapper;
import com.lph.health.service.ReportService;
import com.lph.health.utils.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 运营数据统计
 */
@Service
@Slf4j
public class ReportServiceImpl implements ReportService {

    @Autowired
    private TMemberMapper memberMapper;

    @Autowired
    private TOrderMapper orderMapper;

    /**
     * 获取运营统计数据
     * @return
     */
    @Override
    public Map<String, Object> getBusinessReportData() throws Exception {
        Map<String, Object> map = new HashMap<>();
        // 今日
        String today = DateUtil.parseDate2String(DateUtil.getToday());
        // 获取本周周一的日期
        String thisWeekMonday = DateUtil.parseDate2String(DateUtil.getThisWeekMonday());
        // 获取本月的第一天
        String monthOneDay = DateUtil.parseDate2String(DateUtil.getFirstDay4ThisMonth());


        // reportDate 报表产生日期
        map.put("reportDate", today);
        // todayNewMember 今日新增会员数

        Integer todayNewMember = memberMapper.findMemberCountBeforeDate(today);
        map.put("todayNewMember", todayNewMember);

        // totalMember 总会员数 totalMember
        Integer totalMember = memberMapper.findMemberTotalCount();
        map.put("totalMember", totalMember);

        // thisWeekNewMember 本周新增会员数
        Integer thisWeekNewMember = memberMapper.findMemberCountAfterDate(thisWeekMonday);
        map.put("thisWeekNewMember", thisWeekNewMember);

        // thisMonthNewMember 本月新增会员数
        Integer thisMonthNewMember = memberMapper.findMemberCountAfterDate(monthOneDay);
        map.put("thisMonthNewMember", thisMonthNewMember);

        // todayOrderNumber 今日预约数
        Integer todayOrderNumber = orderMapper.findOrderCountByDate(today);
        map.put("todayOrderNumber", todayOrderNumber);

        // todayVisitsNumber 今日到诊数
        Integer todayVisitsNumber = orderMapper.findVisitsCountByDate(today);
        map.put("todayVisitsNumber", todayVisitsNumber);

        // thisWeekOrderNumber 本周预约数
        Integer thisWeekOrderNumber = orderMapper.findOrderCountAfterDate(thisWeekMonday);
        map.put("thisWeekOrderNumber", thisWeekOrderNumber);

        // thisWeekVisitsNumber 本周到诊数
        Integer thisWeekVisitsNumber = orderMapper.findVisitsCountAfterDate(thisWeekMonday);
        map.put("thisWeekVisitsNumber", thisWeekVisitsNumber);

        // thisMonthOrderNumber 本月预约数
        Integer thisMonthOrderNumber = orderMapper.findOrderCountAfterDate(monthOneDay);
        map.put("thisMonthOrderNumber", thisMonthOrderNumber);

        // thisMonthVisitsNumber 本月到诊数
        Integer thisMonthVisitsNumber = orderMapper.findVisitsCountAfterDate(monthOneDay);
        map.put("thisMonthVisitsNumber", thisMonthVisitsNumber);

        // hotSetmeal 热门套餐 （取前4）
        List<Map> hotSetmeal = orderMapper.findHotSetmeal();
        map.put("hotSetmeal", hotSetmeal);

        return map;
    }
}
