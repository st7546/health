package com.lph.health.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lph.health.dao.BodyEvaluateMapper;
import com.lph.health.dao.SysUserMapper;
import com.lph.health.dao.TOrderMapper;
import com.lph.health.entity.BodyEvaluate;
import com.lph.health.entity.SysUser;
import com.lph.health.entity.TOrder;
import com.lph.health.service.BodyEvaluateService;
import com.lph.health.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-11-17
 */
@Service
@Slf4j
public class BodyEvaluateServiceImpl implements BodyEvaluateService {

    @Autowired
    private BodyEvaluateMapper bodyEvaluateMapper;

    @Autowired
    private TOrderMapper orderMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Override
    public Result findPage(QueryInfo queryInfo) {
        log.info("开始权限数据分页--->页码{}, ---{}页数, --->{}查询内容", queryInfo.getPageNumber(), queryInfo.getPageSize(), queryInfo.getQueryString());
        PageHelper.startPage(queryInfo.getPageNumber(), queryInfo.getPageSize());
        Page<Map> page = bodyEvaluateMapper.findPage(queryInfo.getQueryString());
        long total = page.getTotal();
        log.info("查询的总条数-->{}", total);
        List<Map> result = page.getResult();
        log.info("分页列表-->{}", result);
        return PageResult.pageResult(total, result);
    }

    @Override
    public Result insert(BodyEvaluate bodyEvaluate) {
        TOrder order = orderMapper.findById(bodyEvaluate.getOrderId());
        log.info("改变预约订单到诊状态、评估状态");
        if(order.getBdStatus().equals("已完成")&&order.getBoEvStatus().equals("已完成")&&order.getMeEvStatus().equals("已完成")){
            order.setEvStatus("评估成功");
        }
        order.setOrderStatus("已到诊");
        order.setBoEvStatus("已完成");
        orderMapper.editEvStatusByOrderId(order);
        bodyEvaluateMapper.insert(bodyEvaluate);
        return Result.success("添加体质评估数据成功");
    }

    @Override
    public Result update(BodyEvaluate bodyEvaluate) {
        bodyEvaluateMapper.update(bodyEvaluate);
        return Result.success("修改体质评估数据成功");
    }

    @Override
    public Result findAssessor() {
        List<SysUser> user = sysUserMapper.findAssessor();
//        Map map = new HashMap();
//        SysUser user = new SysUser();
//        userName.forEach( item ->{
//            log.info("输出username"+item.getUsername());
//            map.put("name", item.getUsername());
//        });
//        log.info("name"+map);
        return Result.success("查询健康员成功!" , user);
    }

    @Override
    public Result findBeByBeId(Long id) {
        return Result.success("获取体质评估数据成功!", bodyEvaluateMapper.findBeByBeId(id));
    }


}
