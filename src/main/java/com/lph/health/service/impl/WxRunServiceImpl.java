package com.lph.health.service.impl;

import com.lph.health.dao.WxRunMapper;
import com.lph.health.entity.WxRun;
import com.lph.health.service.WxRunService;
import com.lph.health.utils.Date2Util;
import com.lph.health.utils.Result;
import com.lph.health.utils.SecurityUtil;
import com.lph.health.vo.RunVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-11-27
 */
@Service
@Slf4j
public class WxRunServiceImpl implements WxRunService {

    @Autowired
    private WxRunMapper wxRunMapper;

    @Override
    public Result insert(WxRun wxRun) {
        try {
            WxRun run = wxRunMapper.findByDate(wxRun.getRunDate());
            if (null == run) {
                wxRunMapper.insert(wxRun);
            }
            return Result.success("微信步数录入成功");
        } catch (Exception e) {
            log.info("程序异常 --> {}", e.getMessage());
            return Result.failure("微信步数录入失败");
        }
    }

    @Override
    public Result update(WxRun wxRun) {
        try {
            wxRunMapper.update(wxRun);
            return Result.success("微信步数更新成功");
        } catch (Exception e) {
            log.info("程序异常 --> {}", e.getMessage());
            return Result.failure("微信步数更新失败");
        }
    }

    @Override
    public Result findRun(String date) {
        Long userId = SecurityUtil.getUserId();
        try {
            Map<String, List<WxRun>> map = new HashMap<>(4);
            //从今天起第一周  获取今天是星期几 <---> 周一
            int week1 = Date2Util.getWeekOfDate(date);
            //周一
            String date1 = Date2Util.getWeekBeforeDate(date, week1-1);
            List<WxRun> runs1 = wxRunMapper.findByTime(date, date1, userId);
            map.put("week1", runs1);
            //获取第二周 周天 <---> 周一
            String date2 = Date2Util.getWeekBeforeDate(date1, 1);
            String week2 = Date2Util.getWeekBeforeDate(date2, 6);
            List<WxRun> runs2 = wxRunMapper.findByTime(date2, week2, userId);
            map.put("week2", runs2);
            //第三周 周天 --- 周一
            String date3 = Date2Util.getWeekBeforeDate(week2, 1);
            String week3 = Date2Util.getWeekBeforeDate(date3, 6);
            List<WxRun> runs3 = wxRunMapper.findByTime(date3, week3, userId);
            map.put("week3", runs3);
            //第四周 周天 --- 周一
            String date4 = Date2Util.getWeekBeforeDate(date3, 1);
            String week5 = Date2Util.getWeekBeforeDate(date4, 6);
            List<WxRun> runs4 = wxRunMapper.findByTime(date4, week5, userId);
            map.put("week4", runs4);
            return Result.success("微信步数查询成功", map);
        } catch (Exception e) {
            log.info("程序异常 --> {}", e.getMessage());
            return Result.failure("微信步数查询失败");
        }
    }

    @Override
    public Result findByDate(String stampToDate) {
        try {
            WxRun run = wxRunMapper.findByDate(stampToDate);
            return Result.success("微信步数查询成功", run);
        } catch (Exception e) {
            log.info("程序异常 --> {}", e.getMessage());
            return Result.failure("微信步数查询失败");
        }
    }

    @Override
    public Result getRunInfo(RunVo runVo) {
        try {
            List<WxRun> runs = wxRunMapper.findByTime(runVo.getBeginTime(), runVo.getEndTime(), runVo.getUserId());
            return Result.success("微信步数查询成功", runs);
        } catch (Exception e) {
            log.info("程序异常 --> {}", e.getMessage());
            return Result.failure("微信步数查询失败");
        }
    }
}
