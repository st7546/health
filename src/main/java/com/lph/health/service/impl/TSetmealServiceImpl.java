package com.lph.health.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lph.health.dao.TCheckGroupMapper;
import com.lph.health.dao.TSetmealMapper;
import com.lph.health.entity.TCheckGroup;
import com.lph.health.entity.TSetmeal;
import com.lph.health.service.TSetmealService;
import com.lph.health.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;


/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-10-15
 */

@Service
@Slf4j
public class TSetmealServiceImpl implements TSetmealService {

    @Autowired
    private TSetmealMapper tSetmealMapper;

    @Autowired
    private TCheckGroupMapper tCheckGroupMapper;

    @Autowired
    private RedisUtil redisUtil;

    /**
     * 分页查询
     * @param queryInfo 页码 页数大小 查询内容
     * @return
     */
    @Override
    public Result findPage(QueryInfo queryInfo) {
        log.info("开始菜单数据分页--->页码{}, ---{}页数, --->{}查询内容", queryInfo.getPageNumber(), queryInfo.getPageSize(), queryInfo.getQueryString());
        PageHelper.startPage(queryInfo.getPageNumber(), queryInfo.getPageSize());
        Page<TSetmeal> page = tSetmealMapper.findPage(queryInfo.getQueryString());
        long total = page.getTotal();
        log.info("查询的总条数-->{}", total);
        List<TSetmeal> result = page.getResult();
        result.forEach(item ->{
            // 查询套餐下的检查组信息
            List<TCheckGroup> tCheckGroups = tCheckGroupMapper.findBySetmealId(item.getId());
            item.setCheckGroups(tCheckGroups);
        });
        log.info("分页列表-->{}", result);
        return PageResult.pageResult(total, result);
    }

    /**
     * 角色数据添加
     * @param tSetmeal
     * @Transactional 事物注解
     * @return
     */
    @Transactional
    @Override
    public Result insert(TSetmeal tSetmeal) {
        // 先查询一次角色信息判断其是否存在
        log.info("查询套餐信息是否存在");
        TSetmeal setmeal = tSetmealMapper.findByCode(tSetmeal.getCode());
        if(setmeal != null) {
            return Result.failure("该套餐已经存在!");
        }
        //  需要返回一个最新的套餐id
        tSetmealMapper.insert(tSetmeal);
        // 插入角色信息后完成
        if (tSetmeal.getCheckGroups().size() > 0) {
            tSetmeal.getCheckGroups().forEach(item ->{
                log.info("添加对应套餐数据");
                tSetmealMapper.insertCheckGroup(tSetmeal.getId(), item.getId());
            });
        }
        redisUtil.delKey("userInfo_" + SecurityUtil.getUserName());
        return Result.success("添加套餐信息成功！");
    }

    /**
     * 角色数据删除
     * @param id
     * @return
     */
    @Override
    public Result delete(Long id) {
        // 先查询一次角色信息判断其是否存在
        log.info("查询套餐信息是否存在");
        TSetmeal tSetmeal = tSetmealMapper.findById(id);
        if(tSetmeal == null) {
            return Result.failure("该套餐不存在!");
        }
        log.info("查询该套餐信息下是否有检查项");
        List<TCheckGroup> checkGroups = tCheckGroupMapper.findBySetmealId(id);
        if(checkGroups.size() > 0) {
            return Result.failure("删除失败,该套餐下拥有套餐信息,请先删除对应的套餐信息");
        }
        tSetmealMapper.delete(id);
        redisUtil.delKey("userInfo_" + SecurityUtil.getUserName());
        return Result.success("删除套餐成功");
    }

    /**
     * 角色数据修改
     * @param tSetmeal
     * @return
     */
    @Transactional
    @Override
    public Result update(TSetmeal tSetmeal) {
        // 先根据套餐名称查询一次套餐信息判断其是否存在
        log.info("查询更新的角色信息是否存在");
        TSetmeal setmeal = tSetmealMapper.findByCode(tSetmeal.getCode());
        if(setmeal == null || tSetmeal.getName().equals(tSetmealMapper.findById(tSetmeal.getId()).getName())) {
            if (tSetmeal.getCheckGroups().size() > 0) {
                log.info("先删除对应套餐数据");
                // 删除所有跟该套餐相关的套餐数据
                tSetmealMapper.deleteCheckGroupBySetmalId(tSetmeal.getId());
                tSetmeal.getCheckGroups().forEach(item ->{
                    log.info("再添加对应套餐数据");
                    // 这里的Id是前端传来的
                    tSetmealMapper.insertCheckGroup(tSetmeal.getId(), item.getId());
                });
            } else {
                // 删除所有跟该套餐相关的套餐数据  这里是代表没有选择检查组
                log.info("没有选择 删除全部");
                tSetmealMapper.deleteCheckGroupBySetmalId(tSetmeal.getId());
            }
        } else {
            return Result.failure("该套餐已经存在!");
        }
        tSetmealMapper.update(tSetmeal);
        redisUtil.delKey("userInfo_" + SecurityUtil.getUserName());
        return Result.success("修改套餐信息成功！");
    }

    @Override
    public Result findAll() {
        return Result.success("查询所有套餐成功", tSetmealMapper.findAll());
    }

    @Override
    public Result findGroup(Long id) {
        return Result.success("查询检查组信息成功！", tSetmealMapper.findGroup(id));
    }

    @Override
    public Result findItem(Long id) {
        return Result.success("查询检查项信息成功！", tSetmealMapper.findItem(id));
    }

    @Override
    public Result findById(Long id) {
        TSetmeal tSetmeal = tSetmealMapper.findById(id);
        // 为套餐对象加入对应检查组集合
        tSetmeal.setCheckGroups(tSetmealMapper.findGroup(id));
        // 为套餐对象加入对应检查组对应的检查项集合
        tSetmeal.setCheckItems(tSetmealMapper.findItem(id));
        return Result.success("查询套餐成功！", tSetmeal);
    }

    /**
     * 统计用、获取套餐名称、预约个数
     * 查询主表 为 预约表,副表 套餐表
     * @return
     */
    @Override
    public List<Map<String, Object>> findSetmealCount() {
        return tSetmealMapper.findSetmealCount();
    }
}
