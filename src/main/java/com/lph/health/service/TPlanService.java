package com.lph.health.service;

import com.lph.health.entity.TPlan;
import com.lph.health.utils.QueryInfo;
import com.lph.health.utils.Result;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-11-21
 */
public interface TPlanService{
    /**
     * 修改方案计划
     * @param tPlan
     * @return
     */
    Result updateTPlan(TPlan tPlan);

    /**
     * 删除方案计划
     * @param id
     * @return
     */
    Result deleteTPlan(Long id);

    /**
     * 添加方案计划
     * @param tPlan
     * @return
     */
    Result insertTPlan(TPlan tPlan);

    /**
     * 查询方案
     * @param queryInfo
     * @return
     */
    Result findPlanPage(QueryInfo queryInfo);

    /**
     * 查询未分配食物方案的计划
     * @return
     */
    Result findFoodStatus();


    /**
     * 查询所有分类信息
     * @return
     */
    Result planAll();

    /**
     * 根据计划ID查询套餐信息
     * @param id
     * @return
     */
    Result findPlanById(Long id);

    /**
     * 根据套餐ID查询套餐信息
     * @param id
     * @return
     */
    Result findBySetId(Long id);

    /**
     * 根据套餐ID查询套餐信息
     * @param id
     * @return
     */
    Result findByMerId(Long id);
}
