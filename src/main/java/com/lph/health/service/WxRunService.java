package com.lph.health.service;

import com.lph.health.entity.WxRun;
import com.lph.health.utils.Result;
import com.lph.health.vo.RunVo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-11-27
 */
public interface WxRunService{
    /**
     * 添加微信运动信息
     * @param wxRun
     * @return
     */
    Result insert(WxRun wxRun);

    /**
     * 更新本日步数
     * @param wxRun
     * @return
     */
    Result update(WxRun wxRun);

    /**
     * 查询微信运动信息
     * 传递一个时间 查询4个周的运动数据
     * @return
     */
    Result findRun(String date);

    /**
     * 根据时间查询是否有数据
     * @param stampToDate
     * @return
     */
    Result findByDate(String stampToDate);

    Result getRunInfo(RunVo runVo);
}
