package com.lph.health.service;

import com.lph.health.entity.SysMenu;
import com.lph.health.entity.SysPermission;
import com.lph.health.utils.QueryInfo;
import com.lph.health.utils.Result;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-10-15
 */
public interface SysPermissionService {

    /**
     * 分页查询
     * @param queryInfo 页码 页数大小 查询内容
     * @return
     */
    Result findPage(QueryInfo queryInfo);

    /**
     * 添加权限信息
     * @param sysPermission
     * @return
     */
    Result insert(SysPermission sysPermission);

    /**
     * 删除权限数据
     * @param id
     * @return
     */
    Result delete(Long id);

    /**
     * 修改权限数据
     * @param sysPermission
     * @return
     */
    Result update(SysPermission sysPermission);

    /**
     * 查询全部数据
     * @return
     */
    Result findAll();
}
