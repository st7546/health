package com.lph.health.service;

import com.lph.health.entity.SysRole;
import com.lph.health.entity.TCheckGroup;
import com.lph.health.utils.QueryInfo;
import com.lph.health.utils.Result;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  角色服务类
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-10-15
 */
public interface TChcekGroupService {

    /**
     * 分页查询
     * @param queryInfo 页码 页数大小 查询内容
     * @return
     */
    Result findPage(QueryInfo queryInfo);

    /**
     * 添加角色信息
     * @param tCheckGroup
     * @return
     */
    Result insert(TCheckGroup tCheckGroup);

    /**
     * 删除角色数据
     * @param id
     * @return
     */
    Result delete(Long id);

    /**
     * 修改角色数据
     * @param tCheckGroup
     * @return
     */
    Result update(TCheckGroup tCheckGroup);

    /**
     * 查询所有角色数据
     * @return
     */
    Result findAll();

    /**
     * 根据检查组Id查询检查项信息
     * @param id
     * @return
     */
    Result findItem(@Param("checkGroupId") Long id);
}
