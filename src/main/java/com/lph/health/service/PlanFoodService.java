package com.lph.health.service;


import com.lph.health.entity.PlanFood;
import com.lph.health.utils.QueryInfo;
import com.lph.health.utils.Result;

/**
 * <p>
 *  计划服务类
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-11-21
 */
public interface PlanFoodService {
    /**
     * 删除食物方案
     * @param id
     * @return
     */
    Result delete(Long id);

    /**
     * 修改食物方案
     * @param planFood
     * @return
     */
    Result update(PlanFood planFood);

    /**
     * 添加食物方案
     * @param planFood
     * @return
     */
    Result insert(PlanFood planFood);

    /**
     * 分页查询食物方案信息
     * @param queryInfo
     * @return
     */
    Result findPlanFoodPage(QueryInfo queryInfo);

    Result findByPlanId(QueryInfo queryInfo);
}
