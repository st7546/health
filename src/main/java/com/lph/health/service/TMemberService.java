package com.lph.health.service;


import com.lph.health.entity.TMember;
import com.lph.health.utils.Result;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-11-13
 */
public interface TMemberService{
    /**
     * 根据邮箱查询会员
     * @param email
     * @return
     */
    TMember findByEmail(String email);

    /**
     * 新增会员
     * @param tMember
     */
    void insert(TMember tMember);

    /**
     * 根据月份查询会员数量
     * @param months
     * @return
     */
    List<Integer> findMemberCountByMonths(List<String> months);

    Result findByMerId(@Param("memberId") Long id);
}
