package com.lph.health.service;


import com.lph.health.entity.Food;
import com.lph.health.entity.FoodType;
import com.lph.health.utils.QueryInfo;
import com.lph.health.utils.Result;

import java.util.List;

/**
 * 食品
 */
public interface FoodService {
    /**
     * 删除食品
     * @param id
     * @return
     */
    Result delete(Long id);

    /**
     * 修改食品
     * @param food
     * @return
     */
    Result update(Food food);

    /**
     * 添加食品
     * @param food
     * @return
     */
    Result insert(Food food);

    /**
     * 分页查询食品信息
     * @param queryInfo
     * @return
     */
    Result findPage(QueryInfo queryInfo);

    /**
     * 修改食品分类
     * @param foodType
     * @return
     */
    Result updateType(FoodType foodType);

    /**
     * 删除食物分类
     * @param id
     * @return
     */
    Result deleteType(Long id);

    /**
     * 添加食物分类
     * @param foodType
     * @return
     */
    Result insertType(FoodType foodType);

    /**
     * 批量导入
     * @param list
     * @return
     */
    Result batchImport(List<Food> list);

    /**
     * 查询食物
     * @param queryInfo
     * @return
     */
    Result findFoodPage(QueryInfo queryInfo);

    /**
     * 查询所有分类信息
     * @return
     */
    Result typeAll();

    Result findFoodByTypeId(QueryInfo queryInfo);

    /**
     * 获取分类食物
     * @param id
     * @return
     */
    Result findByTypeId(Long id);
}
