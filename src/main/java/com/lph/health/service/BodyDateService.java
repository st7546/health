package com.lph.health.service;

import com.lph.health.entity.BodyDate;
import com.lph.health.entity.BodyEvaluate;
import com.lph.health.utils.QueryInfo;
import com.lph.health.utils.Result;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 墨浅清谈
 * @since 2021-11-19
 */
public interface BodyDateService {

    /**
     * 分页查询
     * @param queryInfo 页码 页数大小 查询内容
     * @return
     */
    Result findPage(QueryInfo queryInfo);

    /**
     * 添加体质评估信息
     * @param bodyDate
     * @return
     */
    Result insert(BodyDate bodyDate);

    /**
     * 修改体质评估信息
     * @param bodyDate
     * @return
     */
    Result update(BodyDate bodyDate);

    /**
     * 根据评估Id查询评估信息
     */
    Result findBdByBdId(Long id);
}
