package com.lph.health;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class HealthApplication {
    private static final Logger log = LoggerFactory.getLogger(HealthApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(HealthApplication.class, args);
        log.info("HealthApplication 启动!");
    }
}
 